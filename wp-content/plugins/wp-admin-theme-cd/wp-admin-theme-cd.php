<?php /*
Plugin Name: WP Admin Theme CD
Plugin URI: http://www.creative-dive.de
Description: A clean and modern WordPress Admin Theme.
Version: 1.8
Author: Martin Jost
Author URI: http://www.creative-dive.de
Text Domain: wp-admin-theme-cd
Domain Path: /languages
*/

define( 'WP_ADMIN_THEME_CD_VER', '1.8' ); // update plugin main file version number manually
define( 'WP_ADMIN_THEME_CD_PLUGIN_NAME', 'WP Admin Theme CD' );
define( 'WP_ADMIN_THEME_CD_ENVATO_ID', '20354956' );
define( 'WP_ADMIN_THEME_CD_ENVATO_URL', 'https://codecanyon.net/item/wp-admin-theme-cd-a-clean-and-modern-wordpress-admin-theme/20354956' );
define( 'WP_ADMIN_THEME_CD_ENVATO_THEME_REVIEW_URL', 'http://codecanyon.net/downloads#item-' . WP_ADMIN_THEME_CD_ENVATO_ID );
define( 'WP_ADMIN_THEME_CD_AUTHOR_MAIL', 'info@creative-dive.de' );

/*****************************************************************/
/* CREATE PLUGIN PATHS */
/*****************************************************************/

/* INFO: By adding custom wp filter, this plugin can be called from theme folder without installing it manually */

if ( ! function_exists( 'wp_admin_theme_cd_path' ) ) :

    function wp_admin_theme_cd_path( $path ) {
        
		// Get custom filter path	
        if( has_filter( 'wp_admin_theme_cd_path' ) ) {
			return apply_filters( 'wp_admin_theme_cd_path', $path );
		}
        
		// Get plugin path
		return plugins_url( $path , __FILE__ );
        
    }

endif;


if ( ! function_exists( 'wp_admin_theme_cd_dir' ) ) :

    function wp_admin_theme_cd_dir( $path ) {

		// Get custom filter dir path
        if( has_filter( 'wp_admin_theme_cd_dir' ) ) {
			return apply_filters( 'wp_admin_theme_cd_dir', $path );	
		}
        
		// Get plugin dir path
		return plugin_dir_path( __FILE__ ) . $path;
        
    }

endif;


/*****************************************************************/
/* CREATE THE PLUGIN */
/*****************************************************************/

if( ! class_exists('WP_Admin_Theme_CD_Options') ) :

	class WP_Admin_Theme_CD_Options {

		/*****************************************************************/
		/* ATTRIBUTES */
		/*****************************************************************/

		// Refers to a single instance of this class.
		private static $instance = null;

		// Saved options
		public $options;


		/*****************************************************************/
		/* CONSTRUCTOR */
		/*****************************************************************/

		// Creates or returns an instance of this class.
		public static function get_instance() {

			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;

		} // end get_instance;


		/*****************************************************************/
		/* INITIALIZES THE PLUGIN */
		/*****************************************************************/

		public function __construct() {

			if( is_admin() ) {

				require_once( ABSPATH . 'wp-admin/includes/screen.php' );

				// Add textdomain
				load_plugin_textdomain( 'wp-admin-theme-cd', null, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
				
				// Add the page to the admin menu
				add_action( 'admin_menu', array( $this, 'wp_admin_theme_cd_add_page' ) );

				// Register settings options
				add_action( 'admin_init', array( $this, 'wp_admin_theme_cd_register_settings') );

				// Register page options
				add_action( 'admin_init', array( $this, 'wp_admin_theme_cd_register_page_options') );
				
				// Register plugin page scripts
				add_action( 'admin_enqueue_scripts', array( $this, 'wp_admin_theme_cd_load_plugin_page_specific_scripts') ); 

				// Register global javascript and stylesheets
				add_action( 'admin_enqueue_scripts', array( $this, 'wp_admin_theme_cd_enqueue_admin_js' ) );

				// Register generate admin css files
				add_action( 'admin_init', array( $this, 'wp_admin_theme_cd_generate_custom_admin_css' ) );
				
				// Register generate login css files
				add_action( 'admin_init', array( $this, 'wp_admin_theme_cd_generate_custom_login_css' ) );
				
			}

			// Set all option field names
			$this->option_fields = array(
				'user_box' => esc_html__( 'User Box', 'wp-admin-theme-cd' ),
				'company_box' => esc_html__( 'Company Box', 'wp-admin-theme-cd' ),
				'company_box_logo' => esc_html__( 'Company Box Logo', 'wp-admin-theme-cd' ),
				'company_box_logo_size' => esc_html__( 'Company Box Logo Size', 'wp-admin-theme-cd' ),
				'thumbnail' => esc_html__( 'Thumbnails', 'wp-admin-theme-cd' ),
				'post_page_id' => esc_html__( 'Post/Page IDs', 'wp-admin-theme-cd' ),
				'hide_help' => esc_html__( 'Contextual Help', 'wp-admin-theme-cd' ),
				'hide_screen_option' => esc_html__( 'Screen Options', 'wp-admin-theme-cd' ),
				'left_menu_width' => esc_html__( 'Left Menu Width', 'wp-admin-theme-cd' ),
				'left_menu_expand' => esc_html__( 'Left Menu Expandable', 'wp-admin-theme-cd' ),
				'spacing' => esc_html__( 'Spacing', 'wp-admin-theme-cd' ),
				'spacing_max_width' => esc_html__( 'Spacing Max Width', 'wp-admin-theme-cd' ),
				'credits' => esc_html__( 'Credits', 'wp-admin-theme-cd' ),
				'google_webfont' => esc_html__( 'Custom Web Font', 'wp-admin-theme-cd' ),
				'google_webfont_weight' => esc_html__( 'Custom Web Font Weight', 'wp-admin-theme-cd' ),
				'toolbar' => esc_html__( 'Toolbar', 'wp-admin-theme-cd' ),
				'hide_adminbar_comments' => esc_html__( 'Toolbar Comments Menu', 'wp-admin-theme-cd' ),
				'hide_adminbar_new' => esc_html__( 'Toolbar New Content Menu', 'wp-admin-theme-cd' ),
				'hide_adminbar_customize' => esc_html__( 'Toolbar Customize Link', 'wp-admin-theme-cd' ),
				'hide_adminbar_search' => esc_html__( 'Toolbar Search', 'wp-admin-theme-cd' ),
				'toolbar_wp_icon' => esc_html__( 'Toolbar WP Icon', 'wp-admin-theme-cd' ),            
				'toolbar_icon' => esc_html__( 'Custom Toolbar Icon', 'wp-admin-theme-cd' ),         
				'toolbar_color' => esc_html__( 'Toolbar Color', 'wp-admin-theme-cd' ),
				'theme_color' => esc_html__( 'Theme Color', 'wp-admin-theme-cd' ),
				'theme_background' => esc_html__( 'Background Gradient Start Color', 'wp-admin-theme-cd' ),
				'theme_background_end' => esc_html__( 'Background Gradient End Color', 'wp-admin-theme-cd' ),
				'login_disable' => esc_html__( 'Customized Login Page', 'wp-admin-theme-cd' ),
				'login_title' => esc_html__( 'Login Title', 'wp-admin-theme-cd' ),
				'logo_upload' => esc_html__( 'Login Logo', 'wp-admin-theme-cd' ),
				'logo_size' => esc_html__( 'Login Logo Size', 'wp-admin-theme-cd' ),
				'login_bg' => esc_html__( 'Login Background Image', 'wp-admin-theme-cd' ),
				'memory_usage' => esc_html__( 'Memory Usage', 'wp-admin-theme-cd' ),
				'memory_limit' => esc_html__( 'WP Memory Limit', 'wp-admin-theme-cd' ),
				'memory_available' => esc_html__( 'Memory Available', 'wp-admin-theme-cd' ),
				'php_version' => esc_html__( 'PHP Version', 'wp-admin-theme-cd' ),
				'ip_address' => esc_html__( 'IP Address', 'wp-admin-theme-cd' ),
				'wp_version' => esc_html__( 'WP Version', 'wp-admin-theme-cd' ),
				'css_admin' => esc_html__( 'WP Admin CSS', 'wp-admin-theme-cd' ),
				'css_login' => esc_html__( 'WP Login CSS', 'wp-admin-theme-cd' ),
				'wp_svg' => esc_html__( 'SVG Support', 'wp-admin-theme-cd' ),
				'wp_ico' => esc_html__( 'ICO Support', 'wp-admin-theme-cd' ),
				'disable_page_system' => esc_html__( 'WPAT System Info Page', 'wp-admin-theme-cd' ),
				'disable_page_export' => esc_html__( 'WPAT Im- / Export Page', 'wp-admin-theme-cd' ),
				'disable_page_ms' => esc_html__( 'WPAT Multisite Sync Page', 'wp-admin-theme-cd' ),
				'disable_theme_options' => esc_html__( 'Network Theme Options', 'wp-admin-theme-cd' ),
				'wp_version_tag' => esc_html__( 'WP Version Meta-Tag', 'wp-admin-theme-cd' ),
				'wp_emoji' => esc_html__( 'WP Emoji', 'wp-admin-theme-cd' ),
				'wp_feed_links' => esc_html__( 'WP RSS Feed', 'wp-admin-theme-cd' ),
				'wp_rsd_link' => esc_html__( 'WP RSD', 'wp-admin-theme-cd' ),
				'wp_wlwmanifest' => esc_html__( 'WP Wlwmanifest', 'wp-admin-theme-cd' ),
				'wp_shortlink' => esc_html__( 'WP Shortlink', 'wp-admin-theme-cd' ),
				'wp_rest_api' => esc_html__( 'WP REST API', 'wp-admin-theme-cd' ),
				'wp_oembed' => esc_html__( 'WP oEmbed', 'wp-admin-theme-cd' ),
				'wp_xml_rpc' => esc_html__( 'WP XML-RPC / X-Pingback', 'wp-admin-theme-cd' ),
				'wp_heartbeat' => esc_html__( 'WP Heartbeat', 'wp-admin-theme-cd' ),
				'wp_rel_link' => esc_html__( 'WP Rel Links', 'wp-admin-theme-cd' ),
				'wp_self_pingback' => esc_html__( 'WP Self Pingbacks', 'wp-admin-theme-cd' ),
				'mb_custom_fields' => esc_html__( 'Custom Fields Meta Box', 'wp-admin-theme-cd' ),
				'mb_commentstatus' => esc_html__( 'Comments Status Meta Box', 'wp-admin-theme-cd' ),
				'mb_comments' => esc_html__( 'Comments Meta Box', 'wp-admin-theme-cd' ),
				'mb_author' => esc_html__( 'Author Meta Box', 'wp-admin-theme-cd' ),
				'mb_category' => esc_html__( 'Categories Meta Box', 'wp-admin-theme-cd' ),
				'mb_format' => esc_html__( 'Post Format Meta Box', 'wp-admin-theme-cd' ),
				'mb_pageparent' => esc_html__( 'Page Parent Meta Box', 'wp-admin-theme-cd' ),
				'mb_postexcerpt' => esc_html__( 'Post Excerpt Meta Box', 'wp-admin-theme-cd' ),
				'mb_postimage' => esc_html__( 'Post Image Meta Box', 'wp-admin-theme-cd' ),
				'mb_revisions' => esc_html__( 'Revisions Meta Box', 'wp-admin-theme-cd' ),
				'mb_slug' => esc_html__( 'Slug Meta Box', 'wp-admin-theme-cd' ),
				'mb_tags' => esc_html__( 'Tags Meta Box', 'wp-admin-theme-cd' ),
				'mb_trackbacks' => esc_html__( 'Trackbacks Meta Box', 'wp-admin-theme-cd' ),
				'dbw_quick_press' => esc_html__( 'Qick Draft Widget', 'wp-admin-theme-cd' ),
				'dbw_right_now' => esc_html__( 'At the Glance Widget', 'wp-admin-theme-cd' ),
				'dbw_activity' => esc_html__( 'Activity Widget', 'wp-admin-theme-cd' ),
				'dbw_primary' => esc_html__( 'WP Events & News Widget', 'wp-admin-theme-cd' ),
				'dbw_welcome' => esc_html__( 'Welcome Widget', 'wp-admin-theme-cd' ),
				'dbw_wpat_user_log' => esc_html__( 'WPAT User Activities Widget', 'wp-admin-theme-cd' ),
				'dbw_wpat_sys_info' => esc_html__( 'WPAT System Info Widget', 'wp-admin-theme-cd' ),
				'dbw_wpat_count_post' => esc_html__( 'WPAT Post Count Widget', 'wp-admin-theme-cd' ),
				'dbw_wpat_count_page' => esc_html__( 'WPAT Page Count Widget', 'wp-admin-theme-cd' ),
				'dbw_wpat_count_comment' => esc_html__( 'WPAT Comment Count Widget', 'wp-admin-theme-cd' ),
				'dbw_wpat_recent_post' => esc_html__( 'WPAT Recent Posts Widget', 'wp-admin-theme-cd' ),
				'dbw_wpat_recent_page' => esc_html__( 'WPAT Recent Pages Widget', 'wp-admin-theme-cd' ),
				'dbw_wpat_recent_comment' => esc_html__( 'WPAT Recent Comments Widget', 'wp-admin-theme-cd' ),
				'dbw_wpat_memory' => esc_html__( 'WPAT Memory Usage Widget', 'wp-admin-theme-cd' ),
				'wt_pages' => esc_html__( 'Pages Widget', 'wp-admin-theme-cd' ),
				'wt_calendar' => esc_html__( 'Calendar Widget', 'wp-admin-theme-cd' ),
				'wt_archives' => esc_html__( 'Archives Widget', 'wp-admin-theme-cd' ),
				'wt_meta' => esc_html__( 'Meta Widget', 'wp-admin-theme-cd' ),
				'wt_search' => esc_html__( 'Search Widget', 'wp-admin-theme-cd' ),
				'wt_text' => esc_html__( 'Text Widget', 'wp-admin-theme-cd' ),
				'wt_categories' => esc_html__( 'Categories Widget', 'wp-admin-theme-cd' ),
				'wt_recent_posts' => esc_html__( 'Recent Posts Widget', 'wp-admin-theme-cd' ),
				'wt_recent_comments' => esc_html__( 'Recent Comments Widget', 'wp-admin-theme-cd' ),
				'wt_rss' => esc_html__( 'RSS Widget', 'wp-admin-theme-cd' ),
				'wt_tag_cloud' => esc_html__( 'Tag Cloud Widget', 'wp-admin-theme-cd' ),
				'wt_nav' => esc_html__( 'Navigation Menu Widget', 'wp-admin-theme-cd' ),
				'wt_image' => esc_html__( 'Image Widget', 'wp-admin-theme-cd' ),
				'wt_audio' => esc_html__( 'Audio Widget', 'wp-admin-theme-cd' ),
				'wt_video' => esc_html__( 'Video Widget', 'wp-admin-theme-cd' ),
				'wt_gallery' => esc_html__( 'Gallery Widget', 'wp-admin-theme-cd' ),
				'wt_html' => esc_html__( 'Custom HTML Widget', 'wp-admin-theme-cd' ),
				'wp_header_code' => esc_html__( 'Header Code', 'wp-admin-theme-cd' ),
				'wp_footer_code' => esc_html__( 'Footer Code', 'wp-admin-theme-cd' ),
				'meta_referrer_policy' => esc_html__( 'Meta Referrer Policy', 'wp-admin-theme-cd' ),
			);

			$this->plugin_pages_option_fields = array(
				'disable_page_system',
				'disable_page_export',
				'disable_page_ms',      
			);

			$this->optimization_option_fields = array(
				array(
					'wp_version_tag',
					esc_html__( 'Remove the WordPress Version Meta-Tag from wp head.', 'wp-admin-theme-cd' ),
					esc_html__( 'Show the version number of your currently installed WordPress in the source code.', 'wp-admin-theme-cd' ),
				),
				array(
					'wp_emoji',
					esc_html__( 'Remove the WordPress Emoticons from your source code.', 'wp-admin-theme-cd' ),
					esc_html__( 'Display a textual portrayals like ";-)" as a emoticon icon.', 'wp-admin-theme-cd' ),
				),
				array(
					'wp_feed_links',
					esc_html__( 'Disable the RSS feed functionality and remove the WordPress page and comments RSS feed links from wp head.', 'wp-admin-theme-cd' ),
					esc_html__( 'RSS (Really Simple Syndication) is a type of web feed which allows users to access updates to online content in a standardized, computer-readable format.', 'wp-admin-theme-cd' ),
				),
				array(
					'wp_rsd_link',
					esc_html__( 'Remove the RSD link from wp head.', 'wp-admin-theme-cd' ),
					esc_html__( 'Really Simple Discovery (RSD) is an XML format and a publishing convention for making services exposed by a blog, or other web software, discoverable by client software.', 'wp-admin-theme-cd' ),
				),
				array(
					'wp_wlwmanifest',
					esc_html__( 'Remove the Wlwmanifest link from wp head.', 'wp-admin-theme-cd' ),
					esc_html__( 'Needed to enable tagging support for Windows Live Writer.', 'wp-admin-theme-cd' ),
				),
				array(
					'wp_shortlink',
					esc_html__( 'Remove the shortlink link from wp head.', 'wp-admin-theme-cd' ),
					esc_html__( 'Shortlink is a shorten version of a web page’s URL.', 'wp-admin-theme-cd' ),
				),
				array(
					'wp_rest_api',
					esc_html__( 'Disable the REST API and remove the wp json link from wp head.', 'wp-admin-theme-cd' ),
					esc_html__( 'The API makes it super easy to retrieve data using GET requests, which is useful for those building apps with WordPress.', 'wp-admin-theme-cd' ),
				),
				array(
					'wp_oembed',
					esc_html__( 'Disable wp embed and remove the oEmbed links from wp head.', 'wp-admin-theme-cd' ),
					esc_html__( 'oEmbed feature which allows others to embed your WordPress posts into their own site by adding the post URL.', 'wp-admin-theme-cd' ),
				),
				array(
					'wp_xml_rpc',
					esc_html__( 'Disable remote access.', 'wp-admin-theme-cd' ),
					esc_html__( 'XML-RPC is a remote procedure call which uses XML to encode its calls and HTTP as a transport mechanism. If you want to access and publish to your blog remotely, then you need XML-RPC enabled. XML-RPC protocol is used by WordPress as API for Pingbacks and third-party applications, such as mobile apps, inter-blog communication and popular plugins like JetPack.', 'wp-admin-theme-cd' ),
				),
				array(
					'wp_heartbeat',
					esc_html__( 'Stop the heartbeat updates.', 'wp-admin-theme-cd' ),
					esc_html__( 'The Heartbeat API is a simple server polling API built in to WordPress, allowing near-real-time frontend updates. The heartbeat API allows for regular communication between the users browser and the server. One of the original motivations was to allow for locking posts and warning users when more than one user is attempting to edit a post, or warning the user when their log-in has expired.', 'wp-admin-theme-cd' ),
				),
				array(
					'wp_rel_link',
					esc_html__( 'Remove the post rel index / start / parent / prev / next links from wp head.', 'wp-admin-theme-cd' ),
					esc_html__( 'This feature display the URL of the index, start, parent, previous and next post in the source code.', 'wp-admin-theme-cd' ),
				),    
				array(
					'wp_self_pingback',
					esc_html__( 'Disable WordPress self pingbacks / trackbacks.', 'wp-admin-theme-cd' ),
					esc_html__( 'This will allow you to disable self-pingbacks (messages and comments), which are linking back to your own blog.', 'wp-admin-theme-cd' ),
				),     
			);

			$this->meta_box_option_fields = array(
				array(
					'mb_custom_fields',
					esc_html__( 'Remove the Custom Fields Box for posts and pages.', 'wp-admin-theme-cd' ),
					'',
				),
				array(
					'mb_commentstatus',
					esc_html__( 'Remove the Discussion Box for posts and pages.', 'wp-admin-theme-cd' ),
					'',
				),
				array(
					'mb_comments',
					esc_html__( 'Remove the Comments Box for posts and pages.', 'wp-admin-theme-cd' ),
					'',
				),
				array(
					'mb_author',
					esc_html__( 'Remove the Author Box for posts and pages.', 'wp-admin-theme-cd' ),
					'',
				),
				array(
					'mb_category',
					esc_html__( 'Remove the Category Box for posts.', 'wp-admin-theme-cd' ),
					'',
				),
				array(
					'mb_format',
					esc_html__( 'Remove the Post Format Box for posts.', 'wp-admin-theme-cd' ),
					'',
				),
				array(
					'mb_pageparent',
					esc_html__( 'Remove the Page Attributes Box for pages.', 'wp-admin-theme-cd' ),
					'',
				),
				array(
					'mb_postexcerpt',
					esc_html__( 'Remove the Excerpt Box for posts.', 'wp-admin-theme-cd' ),
					'',
				),
				array(
					'mb_postimage',
					esc_html__( 'Remove the Featured Image Box for posts and pages.', 'wp-admin-theme-cd' ),
					'',
				),
				array(
					'mb_revisions',
					esc_html__( 'Remove the Revisions Box for posts and pages.', 'wp-admin-theme-cd' ),
					'',
				),
				array(
					'mb_slug',
					esc_html__( 'Remove the Slug Box for posts and pages.', 'wp-admin-theme-cd' ),
					esc_html__( 'Caution: Disabling the slug box does not allow you to customize the post or page URL.', 'wp-admin-theme-cd' ),
				),
				array(
					'mb_tags',
					esc_html__( 'Remove the Tags Box for posts.', 'wp-admin-theme-cd' ),
					'',
				),
				array(
					'mb_trackbacks',
					esc_html__( 'Remove the Send Trackbacks Box for posts and pages.', 'wp-admin-theme-cd' ),
					'',
				),            
			);

			$this->db_widget_option_fields = array(
				'dbw_quick_press',
				'dbw_right_now',
				'dbw_activity',
				'dbw_primary',
				'dbw_welcome', 
				'dbw_wpat_user_log', 
				'dbw_wpat_sys_info',
				'dbw_wpat_count_post',
				'dbw_wpat_count_page',
				'dbw_wpat_count_comment',
				'dbw_wpat_recent_post',
				'dbw_wpat_recent_page',
				'dbw_wpat_recent_comment',
				'dbw_wpat_memory',
			);

			$this->widget_option_fields = array(
				'wt_pages',
				'wt_calendar',
				'wt_archives',
				'wt_meta',
				'wt_search',
				'wt_text',
				'wt_categories',
				'wt_recent_posts',
				'wt_recent_comments',
				'wt_rss',
				'wt_tag_cloud',
				'wt_nav',
				'wt_image',
				'wt_audio',
				'wt_video',
				'wt_gallery',
				'wt_html',
			);

			$this->frontend_option_fields = array(
				array(
					'wp_header_code',
					esc_html__( 'Add custom code to the frontend header.', 'wp-admin-theme-cd' ),
					esc_html__( 'Will be inserted into the wp_head hook.', 'wp-admin-theme-cd' ),
				),
				array(
					'wp_footer_code',
					esc_html__( 'Add custom code to the frontend footer.', 'wp-admin-theme-cd' ),
					esc_html__( 'Will be inserted into the wp_footer hook.', 'wp-admin-theme-cd' ),
				),
				array(
					'meta_referrer_policy',
					esc_html__( 'Add the meta referrer tag and select your value.', 'wp-admin-theme-cd' ),
					esc_html__( 'If you use SSL for your website, analytics tools like Google Analytics can not see the referrer by default. For example, if you select "Origin", your referrer will be visible again.', 'wp-admin-theme-cd' ),
				),        
			);

			$this->option_heads = array(
				'head_theme' => esc_html__( 'Theme Options', 'wp-admin-theme-cd' ),
				'head_toolbar' => esc_html__( 'Toolbar', 'wp-admin-theme-cd' ),
				'head_color' => esc_html__( 'Colors', 'wp-admin-theme-cd' ),
				'head_login' => esc_html__( 'Login Page', 'wp-admin-theme-cd' ),
				'head_footer' => esc_html__( 'Footer', 'wp-admin-theme-cd' ),
				'head_css' => esc_html__( 'Custom CSS', 'wp-admin-theme-cd' ),
				'head_media' => esc_html__( 'Media', 'wp-admin-theme-cd' ),
				'head_pages' => esc_html__( 'Pages', 'wp-admin-theme-cd' ),
				'head_ms' => esc_html__( 'Multisite', 'wp-admin-theme-cd' ),
				'head_optimize' => esc_html__( 'Optimization & Security', 'wp-admin-theme-cd' ),
				'head_metabox' => esc_html__( 'Meta Boxes', 'wp-admin-theme-cd' ),
				'head_dashboard' => esc_html__( 'Dashboard Widgets', 'wp-admin-theme-cd' ),
				'head_widget' => esc_html__( 'Widgets', 'wp-admin-theme-cd' ),
				'head_frontend' => esc_html__( 'Frontend', 'wp-admin-theme-cd' ),
			);

			// Exception fields are not restorable
			$css_admin = isset( $this->options['css_admin'] ) ? $this->options['css_admin'] : null;
			$css_login = isset( $this->options['css_login'] ) ? $this->options['css_login'] : null;
			$wp_header_code = isset( $this->options['wp_header_code'] ) ? $this->options['wp_header_code'] : null;
			$wp_footer_code = isset( $this->options['wp_footer_code'] ) ? $this->options['wp_footer_code'] : null;

			// Define pre option values (used for initial plugin load and restore options)
			$this->pre_options = array(
				'user_box' => false,
				'company_box' => false,
				'company_box_logo' => '',
				'company_box_logo_size' => '140',
				'thumbnail' => false,
				'post_page_id' => false,
				'hide_help' => false,
				'hide_screen_option' => false,
				'left_menu_width' => '160',
				'left_menu_expand' => false,
				'spacing' => false,
				'spacing_max_width' => '2000',
				'credits' => false,
				'google_webfont' => '',
				'google_webfont_weight' => '',
				'toolbar' => false,
				'hide_adminbar_comments' => false,
				'hide_adminbar_new' => false,
				'hide_adminbar_customize' => false,
				'hide_adminbar_search' => false,
				'toolbar_wp_icon' => false,
				'toolbar_icon' => '',
				'toolbar_color' => '#32373c',
				'theme_color' => '#4777CD',
				'theme_background' => '#545c63',
				'theme_background_end' => '#32373c',
				'login_disable' => false,
				'login_title' => esc_html__( 'Welcome Back.', 'wp-admin-theme-cd' ),
				'logo_upload' => '',
				'logo_size' => '200',
				'login_bg' => '',
				'memory_usage' => false,
				'memory_limit' => false,
				'memory_available' => false,
				'php_version' => false,
				'ip_address' => false,
				'wp_version' => false,
				'css_admin' => esc_html( $css_admin ),
				'css_login' => esc_html( $css_login ),
				'wp_svg' => false,
				'wp_ico' => false,
				'disable_page_system' => false,
				'disable_page_export' => false,
				'disable_page_ms' => false,
				'disable_theme_options' => false,
				'wp_version_tag' => false,
				'wp_emoji' => false,
				'wp_feed_links' => false,
				'wp_rsd_link' => false,
				'wp_wlwmanifest' => false,
				'wp_shortlink' => false,
				'wp_rest_api' => false,
				'wp_oembed' => false,
				'wp_xml_rpc' => false,
				'wp_heartbeat' => false,
				'wp_rel_link' => false,
				'wp_self_pingback' => false,
				'mb_custom_fields' => false,
				'mb_commentstatus' => false,
				'mb_comments' => false,
				'mb_author' => false,
				'mb_category' => false,
				'mb_format' => false,
				'mb_pageparent' => false,
				'mb_postexcerpt' => false,
				'mb_postimage' => false,
				'mb_revisions' => false,
				'mb_slug' => false,
				'mb_tags' => false,
				'mb_trackbacks' => false,
				'dbw_quick_press' => false,
				'dbw_right_now' => false,
				'dbw_activity' => false,
				'dbw_primary' => false,
				'dbw_welcome' => false,
				'dbw_wpat_user_log' => false,
				'dbw_wpat_sys_info' => false,
				'dbw_wpat_count_post' => false,
				'dbw_wpat_count_page' => false,
				'dbw_wpat_count_comment' => false,
				'dbw_wpat_recent_post' => false,
				'dbw_wpat_recent_page' => false,
				'dbw_wpat_recent_comment' => false,
				'dbw_wpat_memory' => false,
				'wt_pages' => false,
				'wt_calendar' => false,
				'wt_archives' => false,
				'wt_meta' => false,
				'wt_search' => false,
				'wt_text' => false,
				'wt_categories' => false,
				'wt_recent_posts' => false,
				'wt_recent_comments' => false,
				'wt_rss' => false,
				'wt_tag_cloud' => false,
				'wt_nav' => false,
				'wt_image' => false,
				'wt_audio' => false,
				'wt_video' => false,
				'wt_gallery' => false,
				'wt_html' => false,
				'wp_header_code' => esc_html( $wp_header_code ),
				'wp_footer_code' => esc_html( $wp_footer_code ),
				'meta_referrer_policy' => 'none',
			);

			// Get registered options
			$this->options = get_option( 'wp_admin_theme_settings_options' );		
			if( is_multisite() ) {
				$this->options = get_blog_option( get_current_blog_id(), 'wp_admin_theme_settings_options', array() );
			}		
			
			// Check if options not exist
			if( ! $this->options ) {				
				// Set initial options to the index
				if( is_multisite() ) {
					update_blog_option( get_current_blog_id(), 'wp_admin_theme_settings_options', $this->pre_options);
				} else {
					update_option( 'wp_admin_theme_settings_options', $this->pre_options, 'yes' );
				}
			}

		}
		

		/*****************************************************************/
		/* ADD PLUGIN OPTIONS PAGE */
		/*****************************************************************/

		public function wp_admin_theme_cd_add_page() {

			// $page_title, $menu_title, $capability, $menu_slug, $callback_function
			add_submenu_page( 
				'tools.php', esc_html__( 'WP Admin Theme', 'wp-admin-theme-cd' ), esc_html__( 'WP Admin Theme', 'wp-admin-theme-cd' ), 'manage_options', 'wp-admin-theme-cd', array( $this, 'wp_admin_theme_cd_display_page' ) 
			);

		}


		/*****************************************************************/
		/* REGISTER PLUGIN SETTINGS/OPTIONS */
		/*****************************************************************/

		public function wp_admin_theme_cd_register_settings() {

			// option group, option name, sanitize
			register_setting( 
				'__FILE__', 'wp_admin_theme_settings_options', array( $this, 'wp_admin_theme_cd_validate_options' ) 
			);

		}


		/*****************************************************************/
		/* DISPLAY PLUGIN OPTIONS PAGE */
		/*****************************************************************/

		public function wp_admin_theme_cd_display_page() { ?>

			<div class="wrap wpat">

				<h1>
					<?php echo wp_admin_theme_cd_title(); ?>
				</h1> 
				
				<?php if( wp_admin_theme_cd_activation_status() ) {
				
					$active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'display-options'; 
					echo wp_admin_theme_cd_tab_menu( $active_tab ); ?>

					<p>
						<?php esc_html_e( 'Speed up and modify your WordPress backend like a charm. This plugin is the central place to take WordPress design to the next level.', 'wp-admin-theme-cd' ); ?>
					</p>

					<div class="wpat-page-menu">
						<ul<?php if( is_rtl() ) { echo ' dir="rtl"'; } ?>>
							<li><a href="#index_theme"><?php echo $this->option_heads['head_theme']; ?></a></li>
							<li><a href="#index_toolbar"><?php echo $this->option_heads['head_toolbar']; ?></a></li>
							<li><a href="#index_color"><?php echo $this->option_heads['head_color']; ?></a></li>
							<li><a href="#index_login"><?php echo $this->option_heads['head_login']; ?></a></li>
							<li><a href="#index_footer"><?php echo $this->option_heads['head_footer']; ?></a></li>
							<li><a href="#index_css"><?php echo $this->option_heads['head_css']; ?></a></li>
							<li><a href="#index_media"><?php echo $this->option_heads['head_media']; ?></a></li>
							<li><a href="#index_page"><?php echo $this->option_heads['head_pages']; ?></a></li>
							<li><a href="#index_ms"><?php echo $this->option_heads['head_ms']; ?></a></li>
							<li><a href="#index_optimize"><?php echo $this->option_heads['head_optimize']; ?></a></li>
							<li><a href="#index_metabox"><?php echo $this->option_heads['head_metabox']; ?></a></li>
							<li><a href="#index_dashboard"><?php echo $this->option_heads['head_dashboard']; ?></a></li>
							<li><a href="#index_widget"><?php echo $this->option_heads['head_widget']; ?></a></li>
							<li><a href="#index_frontend"><?php echo $this->option_heads['head_frontend']; ?></a></li>
						</ul>
					</div>

					<form action="options.php" method="post" enctype="multipart/form-data">

						<?php $options = get_option( 'wp_admin_theme_settings_options' );
						if( is_multisite() ) {
							$main_blog_id = 1;
							$options = get_blog_option( $main_blog_id, 'wp_admin_theme_settings_options', array() );
						}

						/*
						// print options check
						echo '<pre>';
							print_r($this->options);
						echo '</pre>';
						*/			
			
						// Error message output			
						settings_errors('wp_admin_theme_settings_options');

						// Fields output				
						settings_fields('__FILE__');
						do_settings_sections('__FILE__'); ?>

						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row"></th>
									<td>
										<p class="description">

											<?php // Manage save button visibility			
											if( $options['disable_theme_options'] == false || $options['disable_theme_options'] == true && get_current_blog_id() == 1 ) {
												submit_button( esc_html__( 'Save Changes', 'wp-admin-theme-cd' ), 'button button-primary', 'save', false );
											} else {
												echo '<button class="button" disabled value="">' . esc_html__( 'You have no permissions to change this options!', 'wp-admin-theme-cd' ) . '</button>';
											}

											// Manage restore button visibility
											if( $options['disable_theme_options'] == false || $options['disable_theme_options'] == true && get_current_blog_id() == 1 ) {
												submit_button( esc_html__( 'Restore all', 'wp-admin-theme-cd' ), 'button restore', 'reset', false ); 
											} ?>

										</p>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				
				<?php } else {
					echo wp_admin_theme_cd_plugin_activation_message();
				} ?>

			</div>

		<?php }


		/*****************************************************************/
		/* REGISTER PLUGIN ADMIN PAGE OPTIONS */
		/*****************************************************************/

		public function wp_admin_theme_cd_register_page_options() {

			/**********************************/
			// Add Section for option fields
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section', '<span id="index_theme" class="wpat-page-index"></span>' . $this->option_heads['head_theme'], array( $this, 'wp_admin_theme_cd_display_section' ), '__FILE__' 
			);

				add_settings_field( 'admin_theme_spacing',                  $this->option_fields['spacing'],                        array( $this, 'admin_theme_spacing_settings' ),                 '__FILE__', 'admin_theme_section' ); // Add Spacing Option
				add_settings_field( 'admin_theme_user_box',                 $this->option_fields['user_box'],                       array( $this, 'admin_theme_user_box_settings' ),                '__FILE__', 'admin_theme_section' ); // Add User Box Option
				add_settings_field( 'admin_theme_company_box',              $this->option_fields['company_box'],                    array( $this, 'admin_theme_company_box_settings' ),             '__FILE__', 'admin_theme_section' ); // Add Company Box Option
				add_settings_field( 'admin_theme_thumbnail',                $this->option_fields['thumbnail'],                      array( $this, 'admin_theme_thumbnail_settings' ),               '__FILE__', 'admin_theme_section' ); // Add Thumbnail Option
				add_settings_field( 'admin_theme_post_page_id',             $this->option_fields['post_page_id'],                   array( $this, 'admin_theme_post_page_id_settings' ),            '__FILE__', 'admin_theme_section' ); // Add Post/Page ID Option
				add_settings_field( 'admin_theme_hide_help',                $this->option_fields['hide_help'],                      array( $this, 'admin_theme_hide_help_settings' ),               '__FILE__', 'admin_theme_section' ); // Add Hide the Contextual Help Option
				add_settings_field( 'admin_theme_hide_screen_option',       $this->option_fields['hide_screen_option'],             array( $this, 'admin_theme_hide_screen_option_settings' ),      '__FILE__', 'admin_theme_section' ); // Add Hide the Screen Options
				add_settings_field( 'admin_theme_left_menu_width',       	$this->option_fields['left_menu_width'],             	array( $this, 'admin_theme_left_menu_width_settings' ),      	'__FILE__', 'admin_theme_section' ); // Add Left Menu Width Option
				add_settings_field( 'admin_theme_left_menu_expand',       	$this->option_fields['left_menu_expand'],             	array( $this, 'admin_theme_left_menu_expand_settings' ),      	'__FILE__', 'admin_theme_section' ); // Add Left expandable Menu Option
				add_settings_field( 'admin_theme_google_webfont',           $this->option_fields['google_webfont'],                 array( $this, 'admin_theme_google_webfont_settings' ),          '__FILE__', 'admin_theme_section' ); // Add Google Webfont Option

			/**********************************/
			// Add Section for Toolbar
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_toolbar', '<span id="index_toolbar" class="wpat-page-index"></span>' . $this->option_heads['head_toolbar'], array( $this, 'wp_admin_theme_cd_display_section_toolbar' ), '__FILE__' 
			);		

				add_settings_field( 'admin_theme_toolbar',                  $this->option_fields['toolbar'],                        array( $this, 'admin_theme_toolbar_settings' ),                 '__FILE__', 'admin_theme_section_toolbar' ); // Add Hide Toolbar Option            
				add_settings_field( 'admin_theme_hide_adminbar_comments',   $this->option_fields['hide_adminbar_comments'],         array( $this, 'admin_theme_hide_adminbar_comments_settings' ),  '__FILE__', 'admin_theme_section_toolbar' ); // Add Hide Toolbar Comments Menu            
				add_settings_field( 'admin_theme_hide_adminbar_new',        $this->option_fields['hide_adminbar_new'],              array( $this, 'admin_theme_hide_adminbar_new_settings' ),       '__FILE__', 'admin_theme_section_toolbar' ); // Add Hide Toolbar New Content Menu         
				add_settings_field( 'admin_theme_hide_adminbar_customize',  $this->option_fields['hide_adminbar_customize'],        array( $this, 'admin_theme_hide_adminbar_customize_settings' ), '__FILE__', 'admin_theme_section_toolbar' ); // Add Hide Toolbar Customize Link          
				add_settings_field( 'admin_theme_hide_adminbar_search',     $this->option_fields['hide_adminbar_search'],           array( $this, 'admin_theme_hide_adminbar_search_settings' ),    '__FILE__', 'admin_theme_section_toolbar' ); // Add Hide Toolbar Search   
				add_settings_field( 'admin_theme_toolbar_wp_icon',          $this->option_fields['toolbar_wp_icon'],                array( $this, 'admin_theme_toolbar_wp_icon_settings' ),         '__FILE__', 'admin_theme_section_toolbar' ); // Add Hide Toolbar WP Icon          
				add_settings_field( 'admin_theme_toolbar_icon',             $this->option_fields['toolbar_icon'],                   array( $this, 'admin_theme_toolbar_icon_settings' ),            '__FILE__', 'admin_theme_section_toolbar' ); // Add custom Toolbar Icon

			/**********************************/
			// Add Section for Colors Option
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_color', '<span id="index_color" class="wpat-page-index"></span>' . $this->option_heads['head_color'], array( $this, 'wp_admin_theme_cd_display_section_colors' ), '__FILE__' 
			);		

				add_settings_field( 'admin_theme_color',                    $this->option_fields['theme_color'],                    array( $this, 'admin_theme_color_settings' ),                   '__FILE__', 'admin_theme_section_color' ); // Add custom Theme Color Field            
				add_settings_field( 'admin_theme_background',               esc_html__( 'Background Gradient Color', 'wp-admin-theme-cd' ), array( $this, 'admin_theme_background_settings' ),      '__FILE__', 'admin_theme_section_color' ); // Add custom Theme Background Gradient Color Field
				add_settings_field( 'admin_toolbar_color',                  $this->option_fields['toolbar_color'],                  array( $this, 'admin_toolbar_color_settings' ),                 '__FILE__', 'admin_theme_section_color' ); // Add custom Toolbar Color Field   
			
			/**********************************/
			// Add Section for Login Option
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_login', '<span id="index_login" class="wpat-page-index"></span>' . $this->option_heads['head_login'], array( $this, 'wp_admin_theme_cd_display_section_login' ), '__FILE__' 
			);

				add_settings_field( 'admin_theme_login_disable',            $this->option_fields['login_disable'],                  array( $this, 'admin_theme_login_disable_settings' ),           '__FILE__', 'admin_theme_section_login' ); // Add Login Disable Option            
				add_settings_field( 'admin_theme_login_title',              $this->option_fields['login_title'],                    array( $this, 'admin_theme_login_title_settings' ),             '__FILE__', 'admin_theme_section_login' ); // Add Title Field            
				add_settings_field( 'admin_theme_logo_upload',              $this->option_fields['logo_upload'],                    array( $this, 'admin_theme_logo_upload_settings' ),             '__FILE__', 'admin_theme_section_login' ); // Add Logo Option            
				add_settings_field( 'admin_theme_login_bg',                 $this->option_fields['login_bg'],                       array( $this, 'admin_theme_login_bg_settings' ),                '__FILE__', 'admin_theme_section_login' ); // Add Login BG Image Option

			/**********************************/
			// Add Section for Footer Information Option
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_footer', '<span id="index_footer" class="wpat-page-index"></span>' . $this->option_heads['head_footer'], array( $this, 'wp_admin_theme_cd_display_section_footer' ), '__FILE__' 
			);		

				add_settings_field( 'admin_theme_credits',                  $this->option_fields['credits'],                        array( $this, 'admin_theme_credits_settings' ),                 '__FILE__', 'admin_theme_section_footer' ); // Add Credits Option
				add_settings_field( 'admin_theme_memory_usage',             $this->option_fields['memory_usage'],                   array( $this, 'admin_theme_memory_usage_settings' ),            '__FILE__', 'admin_theme_section_footer' ); // Add Memory Usage Option            
				add_settings_field( 'admin_theme_memory_limit',             $this->option_fields['memory_limit'],                   array( $this, 'admin_theme_memory_limit_settings' ),            '__FILE__', 'admin_theme_section_footer' ); // Add WP Memory Limit Option         
				add_settings_field( 'admin_theme_memory_available',         $this->option_fields['memory_available'],               array( $this, 'admin_theme_memory_available_settings' ),        '__FILE__', 'admin_theme_section_footer' ); // Add Memory Available Option             
				add_settings_field( 'admin_theme_php_version',              $this->option_fields['php_version'],                    array( $this, 'admin_theme_php_version_settings' ),             '__FILE__', 'admin_theme_section_footer' ); // Add PHP Version Option            
				add_settings_field( 'admin_theme_ip_address',               $this->option_fields['ip_address'],                     array( $this, 'admin_theme_ip_address_settings' ),              '__FILE__', 'admin_theme_section_footer' ); // Add IP Address Option            
				add_settings_field( 'admin_theme_wp_version',               $this->option_fields['wp_version'],                     array( $this, 'admin_theme_wp_version_settings' ),              '__FILE__', 'admin_theme_section_footer' ); // Add WP Version Option

			/**********************************/
			// Add Section for Custom CSS
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_css', '<span id="index_css" class="wpat-page-index"></span>' . $this->option_heads['head_css'], array( $this, 'wp_admin_theme_cd_display_section_css' ), '__FILE__' 
			);		

				add_settings_field( 'admin_theme_css_admin',                $this->option_fields['css_admin'],                      array( $this, 'admin_theme_css_admin_settings' ),               '__FILE__', 'admin_theme_section_css' ); // Add Custom CSS for WP Admin Theme            
				add_settings_field( 'admin_theme_css_login',                $this->option_fields['css_login'],                      array( $this, 'admin_theme_css_login_settings' ),               '__FILE__', 'admin_theme_section_css' ); // Add Custom CSS for WP Login

			/**********************************/
			// Add Section for Media Support
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_media', '<span id="index_media" class="wpat-page-index"></span>' . $this->option_heads['head_media'], array( $this, 'wp_admin_theme_cd_display_section_media' ), '__FILE__' 
			);		

				add_settings_field( 'admin_theme_wp_svg',                   $this->option_fields['wp_svg'],                         array( $this, 'admin_theme_wp_svg_settings' ),                  '__FILE__', 'admin_theme_section_media' ); // Add SVG Support            
				add_settings_field( 'admin_theme_wp_ico',                   $this->option_fields['wp_ico'],                         array( $this, 'admin_theme_wp_ico_settings' ),                  '__FILE__', 'admin_theme_section_media' ); // Add ICO Support

			/**********************************/
			// Add Section for Plugin Pages
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_plugin_pages', '<span id="index_page" class="wpat-page-index"></span>' . $this->option_heads['head_pages'], array( $this, 'wp_admin_theme_cd_display_section_plugin_pages' ), '__FILE__' 
			);		

				add_settings_field( 'admin_theme_disable_page_system',      $this->option_fields['disable_page_system'],            array( $this, 'admin_theme_disable_plugin_pages_settings' ),    '__FILE__', 'admin_theme_section_plugin_pages' ); // Add Disable Plugin System Page
				add_settings_field( 'admin_theme_disable_page_export',      $this->option_fields['disable_page_export'],            array( $this, 'admin_theme_disable_plugin_pages_settings' ),    '__FILE__', 'admin_theme_section_plugin_pages' ); // Add Disable Plugin Im-/Export Page
				add_settings_field( 'admin_theme_disable_page_ms',          $this->option_fields['disable_page_ms'],                array( $this, 'admin_theme_disable_plugin_pages_settings' ),    '__FILE__', 'admin_theme_section_plugin_pages' ); // Add Disable Plugin Multisite Sync Page

			/**********************************/
			// Add Section for Multisite Support
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_multisite', '<span id="index_ms" class="wpat-page-index"></span>' . $this->option_heads['head_ms'], array( $this, 'wp_admin_theme_cd_display_section_multisite' ), '__FILE__' 
			);		

				add_settings_field( 'admin_theme_disable_theme_options',    $this->option_fields['disable_theme_options'],          array( $this, 'admin_theme_disable_theme_options_settings' ),   '__FILE__', 'admin_theme_section_multisite' ); // Add Disable Theme Options

			/**********************************/
			// Add Section for Optimization
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_optimization', '<span id="index_optimize" class="wpat-page-index"></span>' . $this->option_heads['head_optimize'], array( $this, 'wp_admin_theme_cd_display_section_optimization' ), '__FILE__' 
			);		

				add_settings_field( 'admin_theme_wp_version_tag',           $this->option_fields['wp_version_tag'],                 array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Remove WP Version Tag            
				add_settings_field( 'admin_theme_wp_emoji',                 $this->option_fields['wp_emoji'],                       array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Remove WP Emoticons            
				add_settings_field( 'admin_theme_wp_feed_links',            $this->option_fields['wp_feed_links'],                  array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Remove WP Feed Links            
				add_settings_field( 'admin_theme_wp_rsd_link',              $this->option_fields['wp_rsd_link'],                    array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Remove WP RSD Link            
				add_settings_field( 'admin_theme_wp_wlwmanifest',           $this->option_fields['wp_wlwmanifest'],                 array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Remove WP Wlwmanifest            
				add_settings_field( 'admin_theme_wp_shortlink',             $this->option_fields['wp_shortlink'],                   array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Remove WP Shortlink            
				add_settings_field( 'admin_theme_wp_rest_api',              $this->option_fields['wp_rest_api'],                    array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Remove WP Rest API            
				add_settings_field( 'admin_theme_wp_oembed',                $this->option_fields['wp_oembed'],                      array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Remove WP oEmbed            
				add_settings_field( 'admin_theme_wp_xml_rpc',               $this->option_fields['wp_xml_rpc'],                     array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Remove WP XML RPC            
				add_settings_field( 'admin_theme_wp_heartbeat',             $this->option_fields['wp_heartbeat'],                   array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Remove WP Heartbeat            
				add_settings_field( 'admin_theme_wp_rel_link',              $this->option_fields['wp_rel_link'],                    array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Remove WP Rel Link          
				add_settings_field( 'admin_theme_wp_self_pingback',         $this->option_fields['wp_self_pingback'],               array( $this, 'admin_theme_wp_optimization_settings' ),         '__FILE__', 'admin_theme_section_optimization' ); // Add Disable Self Pingbacks Link

			/**********************************/
			// Add Section for Meta Boxes
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_meta_boxes', '<span id="index_metabox" class="wpat-page-index"></span>' . $this->option_heads['head_metabox'], array( $this, 'wp_admin_theme_cd_display_section_meta_boxes' ), '__FILE__' 
			);		

				add_settings_field( 'admin_theme_mb_custom_fields',         $this->option_fields['mb_custom_fields'],               array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Custom Field Meta Box
				add_settings_field( 'admin_theme_mb_commentstatus',         $this->option_fields['mb_commentstatus'],               array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Comments Status Meta Box
				add_settings_field( 'admin_theme_mb_comments',              $this->option_fields['mb_comments'],                    array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Comments Meta Box
				add_settings_field( 'admin_theme_mb_author',                $this->option_fields['mb_author'],                      array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Author Meta Box
				add_settings_field( 'admin_theme_mb_category',              $this->option_fields['mb_category'],                    array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Category Meta Box
				add_settings_field( 'admin_theme_mb_format',                $this->option_fields['mb_format'],                      array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Post Format Meta Box
				add_settings_field( 'admin_theme_mb_pageparent',            $this->option_fields['mb_pageparent'],                  array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Page Parent Meta Box
				add_settings_field( 'admin_theme_mb_postexcerpt',           $this->option_fields['mb_postexcerpt'],                 array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Post Excerpt Meta Box
				add_settings_field( 'admin_theme_mb_postimage',             $this->option_fields['mb_postimage'],                   array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Post Image Meta Box
				add_settings_field( 'admin_theme_mb_revisions',             $this->option_fields['mb_revisions'],                   array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Revisions Meta Box
				add_settings_field( 'admin_theme_mb_slug',                  $this->option_fields['mb_slug'],                        array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Slug Meta Box
				add_settings_field( 'admin_theme_mb_tags',                  $this->option_fields['mb_tags'],                        array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Tags Meta Box
				add_settings_field( 'admin_theme_mb_trackbacks',            $this->option_fields['mb_trackbacks'],                  array( $this, 'admin_theme_meta_box_settings' ),                '__FILE__', 'admin_theme_section_meta_boxes' ); // Add Remove Trackbacks Meta Box

			/**********************************/
			// Add Section for Dashboard Widgets
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_db_widgets', '<span id="index_dashboard" class="wpat-page-index"></span>' . $this->option_heads['head_dashboard'], array( $this, 'wp_admin_theme_cd_display_section_db_widgets' ), '__FILE__' 
			);	

				add_settings_field( 'admin_theme_dbw_quick_press',          $this->option_fields['dbw_quick_press'],                array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove Qick Draft Widget
				add_settings_field( 'admin_theme_dbw_right_now',            $this->option_fields['dbw_right_now'],                  array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove At the Glance Widget
				add_settings_field( 'admin_theme_dbw_activity',             $this->option_fields['dbw_activity'],                   array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove Activity Widget
				add_settings_field( 'admin_theme_dbw_primary',              $this->option_fields['dbw_primary'],                    array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove WP Events & News Widget
				add_settings_field( 'admin_theme_dbw_welcome',              $this->option_fields['dbw_welcome'],                    array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove Welcome Widget
				add_settings_field( 'admin_theme_dbw_wpat_user_log',        $this->option_fields['dbw_wpat_user_log'],              array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove WPAT User Activities Widget
				add_settings_field( 'admin_theme_dbw_wpat_sys_info',        $this->option_fields['dbw_wpat_sys_info'],              array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove WPAT System info Widget
				add_settings_field( 'admin_theme_dbw_wpat_count_post',      $this->option_fields['dbw_wpat_count_post'],            array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove WPAT Posts Count Widget
				add_settings_field( 'admin_theme_dbw_wpat_count_page',      $this->option_fields['dbw_wpat_count_page'],            array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove WPAT Pages Count Widget
				add_settings_field( 'admin_theme_dbw_wpat_count_comment',   $this->option_fields['dbw_wpat_count_comment'],         array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove WPAT Comments Count Widget
				add_settings_field( 'admin_theme_dbw_wpat_recent_post',     $this->option_fields['dbw_wpat_recent_post'],           array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove WPAT Recent Posts Widget
				add_settings_field( 'admin_theme_dbw_wpat_recent_page',     $this->option_fields['dbw_wpat_recent_page'],           array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove WPAT Recent Pages Widget
				add_settings_field( 'admin_theme_dbw_wpat_recent_comment',  $this->option_fields['dbw_wpat_recent_comment'],        array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove WPAT Recent Comments Widget
				add_settings_field( 'admin_theme_dbw_wpat_memory',          $this->option_fields['dbw_wpat_memory'],                array( $this, 'admin_theme_db_widgets_settings' ),              '__FILE__', 'admin_theme_section_db_widgets' ); // Add Remove WPAT Memory Usage Widget

			/**********************************/
			// Add Section for Widgets
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_widgets', '<span id="index_widget" class="wpat-page-index"></span>' . $this->option_heads['head_widget'], array( $this, 'wp_admin_theme_cd_display_section_widgets' ), '__FILE__' 
			);	

				add_settings_field( 'admin_theme_wt_pages',                 $this->option_fields['wt_pages'],                       array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Pages Widget
				add_settings_field( 'admin_theme_wt_archives',              $this->option_fields['wt_archives'],                    array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Calendar Widget
				add_settings_field( 'admin_theme_wt_calendar',              $this->option_fields['wt_calendar'],                    array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Archives Widget
				add_settings_field( 'admin_theme_wt_meta',                  $this->option_fields['wt_meta'],                        array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Meta Widget
				add_settings_field( 'admin_theme_wt_search',                $this->option_fields['wt_search'],                      array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Search Widget
				add_settings_field( 'admin_theme_wt_text',                  $this->option_fields['wt_text'],                        array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Text Widget
				add_settings_field( 'admin_theme_wt_categories',            $this->option_fields['wt_categories'],                  array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Categories Widget
				add_settings_field( 'admin_theme_wt_recent_posts',          $this->option_fields['wt_recent_posts'],                array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Recent Posts Widget
				add_settings_field( 'admin_theme_wt_recent_comments',       $this->option_fields['wt_recent_comments'],             array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Recent Comments Widget
				add_settings_field( 'admin_theme_wt_rss',                   $this->option_fields['wt_rss'],                         array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove RSS Widget
				add_settings_field( 'admin_theme_wt_tag_cloud',             $this->option_fields['wt_tag_cloud'],                   array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Tag Cloud Widget
				add_settings_field( 'admin_theme_wt_nav',                   $this->option_fields['wt_nav'],                         array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Navigation Menu Widget
				add_settings_field( 'admin_theme_wt_image',                 $this->option_fields['wt_image'],                       array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Image Widget
				add_settings_field( 'admin_theme_wt_audio',                 $this->option_fields['wt_audio'],                       array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Audio Widget
				add_settings_field( 'admin_theme_wt_video',                 $this->option_fields['wt_video'],                       array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Video Widget
				add_settings_field( 'admin_theme_wt_gallery',               $this->option_fields['wt_gallery'],                     array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Gallery Widget
				add_settings_field( 'admin_theme_wt_html',                  $this->option_fields['wt_html'],                        array( $this, 'admin_theme_widgets_settings' ),                 '__FILE__', 'admin_theme_section_widgets' ); // Add Remove Custom HTML Widget
			
			/**********************************/
			// Add Section for Frontend
			/**********************************/
			
			add_settings_section( 
				'admin_theme_section_frontend', '<span id="index_frontend" class="wpat-page-index"></span>' . $this->option_heads['head_frontend'], array( $this, 'wp_admin_theme_cd_display_section_frontend' ), '__FILE__' 
			);	

				add_settings_field( 'admin_theme_wp_header_code',           $this->option_fields['wp_header_code'],                 array( $this, 'admin_theme_frontend_settings' ),                '__FILE__', 'admin_theme_section_frontend' ); // Add Header Code
				add_settings_field( 'admin_theme_wp_footer_code',           $this->option_fields['wp_footer_code'],                 array( $this, 'admin_theme_frontend_settings' ),                '__FILE__', 'admin_theme_section_frontend' ); // Add Footer Code
				add_settings_field( 'admin_theme_meta_referrer_policy',     $this->option_fields['meta_referrer_policy'],           array( $this, 'admin_theme_frontend_settings' ),                '__FILE__', 'admin_theme_section_frontend' ); // Add Meta Policy

		}


		/*****************************************************************/
		/* ADD JS ONLY FOR PLUGIN OPTIONS PAGE */
		/*****************************************************************/

		function wp_admin_theme_cd_load_plugin_page_specific_scripts( $hook ) {

			// Method to get the page hook
			// wp_die($hook);

			// Load only on admin_toplevel_page?page=mypluginname
			if( $hook != 'tools_page_wp-admin-theme-cd' ) {
				return;
			}

			// Add color picker css
			wp_enqueue_style( 'wp-color-picker' );

			// Add media upload js
			wp_enqueue_media();

			// Add plugin js		
			wp_enqueue_script( 
				'wp_admin_script_plugin', wp_admin_theme_cd_path( 'js/jquery.plugin.js' ), array( 'jquery', 'wp-color-picker' ), null, true 
			);

		}


		/*****************************************************************/
		/* ADD GLOBAL JS / CSS */
		/*****************************************************************/

		public function wp_admin_theme_cd_enqueue_admin_js() {

			// Add admin main style
			wp_enqueue_style( 
				'wp_admin_style_custom', wp_admin_theme_cd_path( 'style.css' ), array(), filemtime( wp_admin_theme_cd_dir( 'style.css' ) ), 'all' 
			);

			// Add admin style less
			wp_enqueue_style( 
				'wpat-style-less',  wp_admin_theme_cd_path( 'css/less/style.less' ), array(), null, 'all'
			);

			// Add admin plugin style less
			wp_enqueue_style( 
				'wpat-plugins-less',  wp_admin_theme_cd_path( 'css/less/plugins.less' ), array(), null, 'all'
			);

			// Add admin block editor style less
			wp_enqueue_style( 
				'wpat-block-editor-less',  wp_admin_theme_cd_path( 'css/less/block-editor.less' ), array(), null, 'all'
			);

			// Add admin rtl style less
			if( is_rtl() ) {			
				wp_enqueue_style( 
					'wpat-rtl-style-less', wp_admin_theme_cd_path( 'css/less/rtl-style.less' ), array(), null, 'all' 
				);		
			}

			// Add custom user css for wp admin
			if( wpat_option('css_admin') ) {
				wp_enqueue_style( 
					'wpat-admin-style', wp_admin_theme_cd_path( 'css/admin.css' ), array(), filemtime( wp_admin_theme_cd_dir( 'css/admin.css' ) ), 'all' 
				);
			}

			// Add admin js		
			wp_enqueue_script( 
				'wp_admin_script_custom', wp_admin_theme_cd_path( 'js/jquery.custom.js' ), array( 'jquery' ), null, true 
			);

			// Avoiding flickering to reorder the first menu item (User Box) for left toolbar
			$custom_css = "#adminmenu li:first-child { display:none }";
			wp_add_inline_style( 'wp_admin_style_custom', $custom_css );
			
			// Add HTML tag style for toolbar hide view
			if( wpat_option('toolbar') && ! wpat_option('spacing') || wpat_option('toolbar') && wpat_option('spacing') ) {
				$toolbar_hide_css = "@media (min-width: 960px) { html.wp-toolbar { padding:0px } }";
				wp_add_inline_style( 'wp_admin_style_custom', $toolbar_hide_css );
			}

		}


		/*****************************************************************/
		/* GENERATE CUSTOM ADMIN CSS */
		/*****************************************************************/

		public function wp_admin_theme_cd_generate_custom_admin_css() {

			if( empty( wpat_option('css_admin') ) ) {
				// Stop if no custom admin css is entered
				return;
			}
			
			global $wp_filesystem;
			WP_Filesystem(); // Initial WP file system

			// Add custom user css for wp admin
			ob_start();
			require_once( wp_admin_theme_cd_dir('css/admin.php') );
			$css = ob_get_clean();
			$wp_filesystem->put_contents( wp_admin_theme_cd_dir('css/admin.css'), $css, 0644 );

		}
		
		
		/*****************************************************************/
		/* GENERATE CUSTOM LOGIN CSS */
		/*****************************************************************/

		public function wp_admin_theme_cd_generate_custom_login_css() {

			if( empty( wpat_option('css_login') ) ) {
				// Stop if no custom login css is entered
				return;
			}
			
			global $wp_filesystem;
			WP_Filesystem(); // Initial WP file system

			// Add custom user css for wp login
			ob_start();
			require_once( wp_admin_theme_cd_dir('css/login.php') );
			$css = ob_get_clean();
			$wp_filesystem->put_contents( wp_admin_theme_cd_dir('css/login.css'), $css, 0644 );

		}


		/*****************************************************************/
		/* VALIDATE ALL OPTION FIELDS */
		/*****************************************************************/

		public function wp_admin_theme_cd_validate_options( $fields ) {

			$validation = array();

			// Validate the following fields        
			$get_all_fields = $this->option_fields;        

			foreach( $get_all_fields as $key => $value ) {

				$field_type = trim( $fields[ $key ] );            

				// Extra check for color fields
				if( $key == 'theme_color' || $key == 'theme_background' || $key == 'theme_background_end' ) {

					// Check color is empty (or cleared by user)
					if( $field_type == false ) {
						// empty value
						$validation[ $key ] = '';

					// Check if is a valid hex color    
					} elseif( false == $this->wp_admin_theme_cd_check_color( $field_type ) ) {                    

						if( $key == 'theme_color' ) {
							$validation[ $key ] = $this->options['theme_color'];
						} elseif( $key == 'theme_background' ) {
							$validation[ $key ] = $this->options['theme_background'];   
						} else {
							$validation[ $key ] = $this->options['theme_background_end'];   
						}

						// Invalid color notice
						if( ! empty( $field_type ) ) {
							add_settings_error(
								'wp_admin_theme_settings_options', 'save_updated', esc_html__( 'Invalid Color for', 'wp-admin-theme-cd' ) . ' ' . $value . '! ' . esc_html__( 'Old values has been restored,', 'wp-admin-theme-cd' ), 'error' 
							);
						} 

					// Get validated new hex code
					} else {

						$validation[ $key ] = $field_type;

					}

				} else {

					// Validate all other fields
					if( $key == 'wp_header_code' || $key == 'wp_footer_code' ) {
						$validation[ $key ] = $field_type;
					} else {
						$validation[ $key ] = strip_tags( stripslashes( $field_type ) );    
					}                

				}

				// Get specific update notice
				if( $validation[ $key ] == $this->options[ $key ] ) {
					// Specific field has been not updated (new value == old value)
					//add_settings_error('wp_admin_theme_settings_options', 'save_updated', esc_html__( 'nichts geändert', 'wp-admin-theme-cd' ), 'error' );
				} else {
					// Specific field has been updated
					if( $field_type == $validation[ $key ] ) {
						add_settings_error(
							'wp_admin_theme_settings_options', 'save_updated', $value . ' ' . esc_html__( 'has been updated.', 'wp-admin-theme-cd' ), 'updated' 
						);
					}
				}

			}

			// Reset all fields to default theme options
			if( isset( $_POST['reset'] ) ) {

				add_settings_error(
					'wp_admin_theme_settings_options', 'reset_error', esc_html__( 'All fields has been restored.', 'wp-admin-theme-cd' ), 'updated' 
				);

				// Restore all options to pre defined values
				return $this->pre_options;

			}

			add_settings_error(
				'wp_admin_theme_settings_options', 'save_updated', esc_html__('Settings saved.', 'wp-admin-theme-cd'), 'updated' 
			);

			// Validate all
			return apply_filters( 'wp_admin_theme_cd_validate_options', $validation, $fields);

		}


		/*****************************************************************/
		/* VALIDATE HEX CODE */
		/*****************************************************************/

		// Function that will check if value is a valid HEX color.
		public function wp_admin_theme_cd_check_color( $value ) {

			// If user insert a HEX color with #
			if( preg_match( '/^#[a-f0-9]{6}$/i', $value ) ) {
				return true;
			}

			return false;
		}


		public function wp_admin_theme_cd_display_section() {
			/* Leave blank */
		}

		public function wp_admin_theme_cd_display_section_toolbar() { 
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_colors() {
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_login() { 
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_footer() { 
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_css() { 
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_media() { 
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_plugin_pages() { 
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_multisite() { 
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_optimization() { 
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_meta_boxes() { 
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_db_widgets() { 
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_widgets() { 
			/* Leave blank */ 
		}

		public function wp_admin_theme_cd_display_section_frontend() { 
			/* Leave blank */ 
		}


		/*****************************************************************/
		/* DISPLAY THE OPTION PAGES SETTINGS FIELDS */
		/*****************************************************************/

		// user box

		public function admin_theme_user_box_settings() {

			global $user_box_is_hidden;
			$user_box_is_hidden = $this->options['user_box'];

			$checked = '';
			if( $this->options['user_box'] ) {
				$checked = 'checked="checked"';	
			}

			$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			if( ! $this->options['user_box'] ) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} ?>

			<input type="checkbox" id="user_box" name="wp_admin_theme_settings_options[user_box]" <?php echo esc_html( $checked ); ?> />

			<label for="user_box">
				<?php echo esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status; ?>
			</label>

			<p class="description">
				<?php echo esc_html__( 'Display the user avatar and name before the left wordpress admin menu', 'wp-admin-theme-cd' ); ?>.
			</p>

		<?php }

		// company box

		public function admin_theme_company_box_settings() {

			if( $this->options['company_box'] ) $checked = ' checked="checked" ';  
			else $checked = '';

			global $user_box_is_hidden;

			if( $user_box_is_hidden ) { 
				echo '<div class="wpat-inactive-option">';
			}

				if( ! $this->options['company_box']) { 
					$field_status = '<span class="field-status hidden">' . esc_html__( 'Disabled', 'wp-admin-theme-cd' ) . '</span>';
				} else {
					$field_status = '<span class="field-status visible">' . esc_html__( 'Enabled', 'wp-admin-theme-cd' ) . '</span>';
				}

				echo '<input type="checkbox" ' . $checked . ' id="company_box" name="wp_admin_theme_settings_options[company_box]" />';

				echo '<label for="company_box">' . esc_html__( 'Enable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

				echo '<p class="description">' . esc_html__( 'Show a company box with your logo instead of the user box. The user box must be visible', 'wp-admin-theme-cd' ) . '.</p>';

				/*******/
				echo '<br>';

				$val = ( isset( $this->options['company_box_logo'] ) ) ? $this->options['company_box_logo'] : '';
				$val2 = ( isset( $this->options['company_box_logo_size'] ) ) ? $this->options['company_box_logo_size'] : '140';

				echo '<label for="company_box_logo">' . esc_html__( 'Company Logo', 'wp-admin-theme-cd' ) . ' </label>';
				echo '<input type="text" id="company_box_logo" name="wp_admin_theme_settings_options[company_box_logo]" value="' . $val . '" />'; 
				echo '<input id="company_box_logo_upload_button" class="button uploader" type="button" value="' . esc_html__( 'Upload Image', 'wp-admin-theme-cd' ) . '" /> ';

				echo '<label class="wpat-nextto-input" for="company_box_logo_size" style="margin-left: 30px">' . esc_html__( 'Logo Size', 'wp-admin-theme-cd' ) . ' </label>';
				echo '<input class="wpat-range-value"  type="range" id="company_box_logo_size" name="wp_admin_theme_settings_options[company_box_logo_size]" value="' . $val2 . '" min="100" max="300" />';
				echo '<span class="wpat-input-range"><span>140</span></span>';
				echo '<label for="company_box_logo_size"> ' . esc_html__( 'Pixel', 'wp-admin-theme-cd' ) . '</label>';

				if( $this->options['company_box_logo'] ) {
					$bg_image = $this->options['company_box_logo'];
				} else {
					$bg_image = wp_admin_theme_cd_path('img/no-thumb.jpg');
				}

				echo '<div class="img-upload-container" style="background-image:url(' . $bg_image . ')"></div>';

			if( $user_box_is_hidden ) { 
				echo '</div>';
			}

		}

		// thumbnails

		public function admin_theme_thumbnail_settings() {

			if( $this->options['thumbnail'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['thumbnail']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="thumbnail" name="wp_admin_theme_settings_options[thumbnail]" />';

			echo '<label for="thumbnail">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Display a thumbnail column before the title for post and page table lists', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// post/page ids

		public function admin_theme_post_page_id_settings() {

			if( $this->options['post_page_id'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['post_page_id']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="post_page_id" name="wp_admin_theme_settings_options[post_page_id]" />';

			echo '<label for="post_page_id">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Display a IDs column for post and page table lists', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// hide contextual help

		public function admin_theme_hide_help_settings() {

			if( $this->options['hide_help'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['hide_help']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="hide_help" name="wp_admin_theme_settings_options[hide_help]" />';

			echo '<label for="hide_help">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Hide the contextual help at the top right side', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// hide screen options

		public function admin_theme_hide_screen_option_settings() {

			if( $this->options['hide_screen_option'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['hide_screen_option']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="hide_screen_option" name="wp_admin_theme_settings_options[hide_screen_option]" />';

			echo '<label for="hide_screen_option">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Hide the screen options at the top right side', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// left menu width

		public function admin_theme_left_menu_width_settings() {

			$val = ( isset( $this->options['left_menu_width'] ) ) ? $this->options['left_menu_width'] : '160';

			echo '<input class="wpat-range-value" type="range" id="left_menu_width" name="wp_admin_theme_settings_options[left_menu_width]" value="' . $val . '" min="160" max="400" />';
			echo '<span class="wpat-input-range"><span>160</span></span>';
			echo '<label for="left_menu_width"> ' . esc_html__( 'Pixel', 'wp-admin-theme-cd' ) . '</label>';

			echo '<p class="description">' . esc_html__( 'Increase the left admin menu width up to 400px', 'wp-admin-theme-cd' ) . '.</p>';

			echo '<small class="wpat-info">' . esc_html__( 'This option may affect the design of some WordPress plugins. If you use many plugins, this option is not recommended.', 'wp-admin-theme-cd' ) . '</small>';

		}

		// left menu expandable options

		public function admin_theme_left_menu_expand_settings() {

			if( $this->options['left_menu_expand'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['left_menu_expand']) { 
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Disabled', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status visible">' . esc_html__( 'Enabled', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="left_menu_expand" name="wp_admin_theme_settings_options[left_menu_expand]" />';

			echo '<label for="left_menu_expand">' . esc_html__( 'Enable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Display submenus of the left admin menu only after clicking as an expandable menu', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// spacing

		public function admin_theme_spacing_settings() {

			global $spacing_is_enabled;
			$spacing_is_enabled = $this->options['spacing'];
			
			$checked = '';
			if( $this->options['spacing'] ) {
				$checked = ' checked="checked" '; 	
			}  

			$val = isset( $this->options['spacing_max_width'] ) ? $this->options['spacing_max_width'] : '2000';			

			$field_status = '<span class="field-status visible">' . esc_html__( 'Enabled', 'wp-admin-theme-cd' ) . '</span>';
			if( ! $this->options['spacing']) { 
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Disabled', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="spacing" name="wp_admin_theme_settings_options[spacing]" />';

			echo '<label for="spacing">' . esc_html__( 'Enable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Add spacing around the backend block', 'wp-admin-theme-cd' ) . '.</p>';

			if( ! $spacing_is_enabled ) { 
				echo '<div class="wpat-inactive-option">';
			}

				/*******/
				echo '<br>';

				echo '<label class="wpat-nextto-input" for="spacing_max_width">' . esc_html__( 'Max Width', 'wp-admin-theme-cd' ) . ' </label>';
				echo '<input class="wpat-range-value"  type="range" id="spacing_max_width" name="wp_admin_theme_settings_options[spacing_max_width]" value="' . $val . '" min="1000" max="2600" />';
				echo '<span class="wpat-input-range"><span>2000</span></span>';
				echo '<label for="spacing_max_width"> ' . esc_html__( 'Pixel', 'wp-admin-theme-cd' ) . '</label>';

			if( ! $spacing_is_enabled ) { 
				echo '</div>';
			}

			echo '<small class="wpat-info">' . esc_html__( 'This option may affect the design of some WordPress plugins. If you use plugins with fixed positioned containers, this option is not recommended.', 'wp-admin-theme-cd' ) . '</small>';

		}

		// credits

		public function admin_theme_credits_settings() {

			if( $this->options['credits'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['credits']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="credits" name="wp_admin_theme_settings_options[credits]" />';

			echo '<label for="credits">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Remove the credits note from the footer', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// google webfont

		public function admin_theme_google_webfont_settings() {

			$val = ( isset( $this->options['google_webfont'] ) ) ? $this->options['google_webfont'] : '';
			$val2 = ( isset( $this->options['google_webfont_weight'] ) ) ? $this->options['google_webfont_weight'] : '';

			echo '<p><input type="text" id="google_webfont" name="wp_admin_theme_settings_options[google_webfont]" value="' . $val . '" size="60" placeholder="Open+Sans" />';

			echo '&nbsp;&nbsp;<label for="google_webfont">' . esc_html__( 'Font-Family', 'wp-admin-theme-cd' ) . '</label></p>';

			echo '<p><input type="text" id="google_webfont_weight" name="wp_admin_theme_settings_options[google_webfont_weight]" value="' . $val2 . '" size="60" placeholder="300,400,400i,700" />';

			echo '&nbsp;&nbsp;<label for="google_webfont_weight">' . esc_html__( 'Font-Weight', 'wp-admin-theme-cd' ) . '</label></p>';

			echo '<p class="description">' . wp_kses( __( 'Embed a custom <a target="_blank" href="https://fonts.google.com/">Google Webfont</a> to your WordPress Admin', 'wp-admin-theme-cd' ), 
				array(  
					'a' => array( 
						'href' => array(),
						'target' => array(),
					) 
				)
			) . '.</p>';

			echo '<small class="wpat-info">' . esc_html__( 'Please separate in Font-Name and Font-Weight like this example: [Font-Family = "Roboto"] and [Font-Weight = "400,400i,700"]', 'wp-admin-theme-cd' ) . '</small>';

		}

		// toolbar

		public function admin_theme_toolbar_settings() {

			if( $this->options['toolbar'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			global $toolbar_is_hidden;
			$toolbar_is_hidden = $this->options['toolbar'];

			if( ! $this->options['toolbar']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="toolbar" name="wp_admin_theme_settings_options[toolbar]" />';

			echo '<label for="toolbar">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Remove the upper toolbar', 'wp-admin-theme-cd' ) . '.</p>';

			echo '<small class="wpat-info">' . esc_html__( 'This option may affect the design of some WordPress plugins. If you use plugins with fixed positioned containers, this option is not recommended.', 'wp-admin-theme-cd' ) . '</small>';

		}

		// toolbar comments menu

		public function admin_theme_hide_adminbar_comments_settings() {

			if( $this->options['hide_adminbar_comments'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			global $toolbar_is_hidden;

			if( $toolbar_is_hidden ) { 
				echo '<div class="wpat-inactive-option">';
			}

				if( ! $this->options['hide_adminbar_comments']) { 
					$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
				} else {
					$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
				}

				echo '<input type="checkbox" ' . $checked . ' id="hide_adminbar_comments" name="wp_admin_theme_settings_options[hide_adminbar_comments]" />';

				echo '<label for="hide_adminbar_comments">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

				echo '<p class="description">' . esc_html__( 'Remove the WordPress Comments Menu from the upper toolbar', 'wp-admin-theme-cd' ) . '.</p>';

			if( $toolbar_is_hidden ) { 
				echo '</div>';
			}

		}

		// toolbar new content menu

		public function admin_theme_hide_adminbar_new_settings() {

			if( $this->options['hide_adminbar_new'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			global $toolbar_is_hidden;

			if( $toolbar_is_hidden ) { 
				echo '<div class="wpat-inactive-option">';
			}

				if( ! $this->options['hide_adminbar_new']) { 
					$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
				} else {
					$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
				}

				echo '<input type="checkbox" ' . $checked . ' id="hide_adminbar_new" name="wp_admin_theme_settings_options[hide_adminbar_new]" />';

				echo '<label for="hide_adminbar_new">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

				echo '<p class="description">' . esc_html__( 'Remove the WordPress New Content Menu from the upper toolbar', 'wp-admin-theme-cd' ) . '.</p>';

			if( $toolbar_is_hidden ) { 
				echo '</div>';
			}

		}

		// toolbar customize link

		public function admin_theme_hide_adminbar_customize_settings() {

			if( $this->options['hide_adminbar_customize'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			global $toolbar_is_hidden;

			if( $toolbar_is_hidden ) { 
				echo '<div class="wpat-inactive-option">';
			}

				if( ! $this->options['hide_adminbar_customize']) { 
					$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
				} else {
					$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
				}

				echo '<input type="checkbox" ' . $checked . ' id="hide_adminbar_customize" name="wp_admin_theme_settings_options[hide_adminbar_customize]" />';

				echo '<label for="hide_adminbar_customize">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

				echo '<p class="description">' . esc_html__( 'Remove the WordPress Customize Link from the upper frontend toolbar', 'wp-admin-theme-cd' ) . '.</p>';

			if( $toolbar_is_hidden ) { 
				echo '</div>';
			}

		}

		// toolbar search

		public function admin_theme_hide_adminbar_search_settings() {

			if( $this->options['hide_adminbar_search'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			global $toolbar_is_hidden;

			if( $toolbar_is_hidden ) { 
				echo '<div class="wpat-inactive-option">';
			}

				if( ! $this->options['hide_adminbar_search']) { 
					$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
				} else {
					$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
				}

				echo '<input type="checkbox" ' . $checked . ' id="hide_adminbar_search" name="wp_admin_theme_settings_options[hide_adminbar_search]" />';

				echo '<label for="hide_adminbar_search">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

				echo '<p class="description">' . esc_html__( 'Remove the WordPress Search from the upper frontend toolbar', 'wp-admin-theme-cd' ) . '.</p>';

			if( $toolbar_is_hidden ) { 
				echo '</div>';
			}

		}

		// toolbar wp icon

		public function admin_theme_toolbar_wp_icon_settings() {

			if( $this->options['toolbar_wp_icon'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			global $toolbar_is_hidden, $toolbar_wp_icon_is_hidden;
			$toolbar_wp_icon_is_hidden = $this->options['toolbar_wp_icon'];

			if( $toolbar_is_hidden ) { 
				echo '<div class="wpat-inactive-option">';
			}

				if( ! $this->options['toolbar_wp_icon']) { 
					$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
				} else {
					$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
				}

				echo '<input type="checkbox" ' . $checked . ' id="toolbar_wp_icon" name="wp_admin_theme_settings_options[toolbar_wp_icon]" />';

				echo '<label for="toolbar_wp_icon">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

				echo '<p class="description">' . esc_html__( 'Remove the WordPress Menu and Icon from the upper toolbar', 'wp-admin-theme-cd' ) . '.</p>';

			if( $toolbar_is_hidden ) { 
				echo '</div>';
			}

		}

		// toolbar custom icon

		public function admin_theme_toolbar_icon_settings() {

			$val = ( isset( $this->options['toolbar_icon'] ) ) ? $this->options['toolbar_icon'] : '';

			global $toolbar_is_hidden, $toolbar_wp_icon_is_hidden;

			if( $toolbar_is_hidden || $toolbar_wp_icon_is_hidden ) { 
				echo '<div class="wpat-inactive-option">';
			}

				echo '<input type="text" id="toolbar_icon" name="wp_admin_theme_settings_options[toolbar_icon]" value="' . $val . '" />'; 
				echo '<input id="toolbar_icon_upload_button" class="button uploader" type="button" value="' . esc_html__( 'Upload Image', 'wp-admin-theme-cd' ) . '" /> ';

				if( $this->options['toolbar_icon'] ) {
					$bg_image = $this->options['toolbar_icon'];
				} else {
					$bg_image = wp_admin_theme_cd_path('img/no-thumb.jpg');
				}

				echo '<div class="img-upload-container" style="background-image:url(' . $bg_image . ')"></div>';
				echo '<p class="description">' . esc_html__( 'Upload a custom icon instead of the WordPress icon', 'wp-admin-theme-cd' ) . '.</p>';

				echo '<small class="wpat-info">' . esc_html__( 'Recommended image size is 26 x 26px.', 'wp-admin-theme-cd' ) . '</small>';

			if( $toolbar_is_hidden || $toolbar_wp_icon_is_hidden ) { 
				echo '</div>';
			}

		}

		// theme color

		public function admin_theme_color_settings() {

			$val = ( isset( $this->options['theme_color'] ) ) ? $this->options['theme_color'] : '#4777CD';
			echo '<input type="text" name="wp_admin_theme_settings_options[theme_color]" value="' . $val . '" class="cpa-color-picker" >';
			echo '<p class="description">' . esc_html__( 'Select your custom wp admin theme color. Default value is #4777CD', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// theme gradient start + end color

		public function admin_theme_background_settings() {

			$val = ( isset( $this->options['theme_background'] ) ) ? $this->options['theme_background'] : '#545c63';
			echo '<input type="text" name="wp_admin_theme_settings_options[theme_background]" value="' . $val . '" class="cpa-color-picker" >';
			echo '<label for="theme_background" class="color-picker">' . esc_html__( 'Start Color', 'wp-admin-theme-cd' ) . '</label>';

			$val2 = ( isset( $this->options['theme_background_end'] ) ) ? $this->options['theme_background_end'] : '#32373c';
			echo '<input type="text" name="wp_admin_theme_settings_options[theme_background_end]" value="' . $val2 . '" class="cpa-color-picker" >';
			echo '<label for="theme_background_end" class="color-picker">' . esc_html__( 'End Color', 'wp-admin-theme-cd' ) . '</label>';

			echo '<p class="description">' . esc_html__( 'Select your custom wp admin theme background gradient color. Default start value is #545c63 and end value is #32373c', 'wp-admin-theme-cd' ) . '.</p>';

		}
		
		// toolbar color

		public function admin_toolbar_color_settings() {

			$val = isset( $this->options['toolbar_color'] ) ? $this->options['toolbar_color'] : '#32373c';
			echo '<input type="text" name="wp_admin_theme_settings_options[toolbar_color]" value="' . $val . '" class="cpa-color-picker" >';
			echo '<p class="description">' . esc_html__( 'Default value is #32373c', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// login disable

		public function admin_theme_login_disable_settings() {

			if( $this->options['login_disable'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			global $login_is_disabled;
			$login_is_disabled = $this->options['login_disable'];

			if( ! $this->options['login_disable']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Enabled', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Disabled', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="login_disable" name="wp_admin_theme_settings_options[login_disable]" />';

			echo '<label for="login_disable">' . esc_html__( 'Disable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'It is useful if you have an other login plugin installed. This is preventing conflicts with other plugins', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// login title

		public function admin_theme_login_title_settings() {

			$val = ( isset( $this->options['login_title'] ) ) ? $this->options['login_title'] : 'Welcome back.';

			global $login_is_disabled;

			if( $login_is_disabled ) { 
				echo '<div class="wpat-inactive-option">';
			}

				echo '<input type="text" name="wp_admin_theme_settings_options[login_title]" value="' . $val . '" size="60" />';

			if( $login_is_disabled ) { 
				echo '</div>';
			}

		}

		// login logo + size

		public function admin_theme_logo_upload_settings() {

			$val = ( isset( $this->options['logo_upload'] ) ) ? $this->options['logo_upload'] : '';
			$val2 = ( isset( $this->options['logo_size'] ) ) ? $this->options['logo_size'] : '200';

			global $login_is_disabled;

			if( $login_is_disabled ) { 
				echo '<div class="wpat-inactive-option">';
			}

				echo '<input type="text" id="logo_upload" name="wp_admin_theme_settings_options[logo_upload]" value="' . $val . '" />'; 
				echo '<input id="logo_upload_button" class="button uploader" type="button" value="' . esc_html__( 'Upload Image', 'wp-admin-theme-cd' ) . '" /> ';

				echo '<label class="wpat-nextto-input" for="logo_size" style="margin-left: 30px">' . esc_html__( 'Logo Size', 'wp-admin-theme-cd' ) . ' </label>';
				echo '<input class="wpat-range-value"  type="range" id="logo_size" name="wp_admin_theme_settings_options[logo_size]" value="' . $val2 . '" min="100" max="400" />';
				echo '<span class="wpat-input-range"><span>200</span></span>';
				echo '<label for="logo_size" class="logo-size"> ' . esc_html__( 'Pixel', 'wp-admin-theme-cd' ) . '</label>';

				if( $this->options['logo_upload'] ) {
					$logo_image = $this->options['logo_upload'];
				} else {
					$logo_image = wp_admin_theme_cd_path('img/no-thumb.jpg');
				}

				echo '<div class="img-upload-container" style="background-image:url(' . $logo_image . ')"></div>';
				echo '<p class="description">' . esc_html__( 'Upload an image for your WordPress login page', 'wp-admin-theme-cd' ) . '.</p>';

			if( $login_is_disabled ) { 
				echo '</div>';
			}

		}

		// login background image

		public function admin_theme_login_bg_settings() {

			$val = ( isset( $this->options['login_bg'] ) ) ? $this->options['login_bg'] : '';

			global $login_is_disabled;

			if( $login_is_disabled ) { 
				echo '<div class="wpat-inactive-option">';
			}

				echo '<input type="text" id="login_bg" name="wp_admin_theme_settings_options[login_bg]" value="' . $val . '" />'; 
				echo '<input id="login_bg_upload_button" class="button uploader" type="button" value="' . esc_html__( 'Upload Image', 'wp-admin-theme-cd' ) . '" /> ';

				if( $this->options['login_bg'] ) {
					$bg_image = $this->options['login_bg'];
				} else {
					$bg_image = wp_admin_theme_cd_path('img/no-thumb.jpg');
				}

				echo '<div class="img-upload-container" style="background-image:url(' . $bg_image . ')"></div>';
				echo '<p class="description">' . esc_html__( 'Upload a background image for your WordPress login page', 'wp-admin-theme-cd' ) . '.</p>';

			if( $login_is_disabled ) { 
				echo '</div>';
			}

		}

		// memory usage

		public function admin_theme_memory_usage_settings() {

			if( $this->options['memory_usage'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['memory_usage']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="memory_usage" name="wp_admin_theme_settings_options[memory_usage]" />';

			echo '<label for="memory_usage">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Display the currently memory usage of your WordPress installation', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// memory limit

		public function admin_theme_memory_limit_settings() {

			if( $this->options['memory_limit'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['memory_limit']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="memory_limit" name="wp_admin_theme_settings_options[memory_limit]" />';

			echo '<label for="memory_limit">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Display the memory limit of your WordPress installation', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// memory available

		public function admin_theme_memory_available_settings() {

			if( $this->options['memory_available'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['memory_available']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="memory_available" name="wp_admin_theme_settings_options[memory_available]" />';

			echo '<label for="memory_available">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Display the available server memory for your WordPress installation', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// php version

		public function admin_theme_php_version_settings() {

			if( $this->options['php_version'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['php_version']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="php_version" name="wp_admin_theme_settings_options[php_version]" />';

			echo '<label for="php_version">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Display the PHP version of your server', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// ip address

		public function admin_theme_ip_address_settings() {

			if( $this->options['ip_address'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['ip_address']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="ip_address" name="wp_admin_theme_settings_options[ip_address]" />';

			echo '<label for="ip_address">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Display the IP address of your server', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// wp version

		public function admin_theme_wp_version_settings() {

			if( $this->options['wp_version'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['wp_version']) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Visible', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Hidden', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="wp_version" name="wp_admin_theme_settings_options[wp_version]" />';

			echo '<label for="wp_version">' . esc_html__( 'Hide', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Display the installed WordPress version', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// theme css

		public function admin_theme_css_admin_settings() {

			$val = ( isset( $this->options['css_admin'] ) ) ? $this->options['css_admin'] : '';
			echo '<textarea class="option-textarea" type="text" name="wp_admin_theme_settings_options[css_admin]" placeholder=".your-class { color: blue }" />' . $val . '</textarea>';

			echo '<p class="description">' . esc_html__( 'Add custom CSS for the Wordpress admin theme. To overwrite some classes, use "!important". Like this example "border-right: 3px!important"', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// login css

		public function admin_theme_css_login_settings() {

			$val = ( isset( $this->options['css_login'] ) ) ? $this->options['css_login'] : '';
			echo '<textarea class="option-textarea" type="text" name="wp_admin_theme_settings_options[css_login]" placeholder=".your-class { color: blue }" />' . $val . '</textarea>';

			echo '<p class="description">' . esc_html__( 'Add custom CSS for the Wordpress login page. To overwrite some classes, use "!important". Like this example "border-right: 3px!important"', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// wp svg

		public function admin_theme_wp_svg_settings() {

			if( $this->options['wp_svg'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['wp_svg'] ) { 
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Deactivated', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status visible">' . esc_html__( 'Activated', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="wp_svg" name="wp_admin_theme_settings_options[wp_svg]" />';

			echo '<label for="wp_svg">' . esc_html__( 'Enable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Allow the upload of SVG files', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// wp ico

		public function admin_theme_wp_ico_settings() {

			if( $this->options['wp_ico'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['wp_ico'] ) { 
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Deactivated', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status visible">' . esc_html__( 'Activated', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="wp_ico" name="wp_admin_theme_settings_options[wp_ico]" />';

			echo '<label for="wp_ico">' . esc_html__( 'Enable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Allow the upload of ICO files', 'wp-admin-theme-cd' ) . '.</p>';

		}

		// remove plugin pages
		/*****************************************************************/

		function admin_theme_disable_plugin_pages_settings() {

			// get all meta box settings fields
			$field = array_shift( $this->plugin_pages_option_fields );

			// Multisite sync page can not be visible, because WordPress multisite is not activated
			if( $field == 'disable_page_ms' ) {
				if( ! is_multisite() ) { 
					echo '<div class="wpat-inactive-option">';
				}
			}

				if( $this->options[$field] ) $checked = ' checked="checked" '; 
				else $checked = '';

				if( $field == 'disable_page_ms' && ! is_multisite() ) {
					$field_status = '<span class="field-status hidden">' . esc_html__( 'Removed', 'wp-admin-theme-cd' ) . '</span>';
				} elseif( ! $this->options[$field] ) { 
					$field_status = '<span class="field-status visible">' . esc_html__( 'Activated', 'wp-admin-theme-cd' ) . '</span>';
				} else {
					$field_status = '<span class="field-status hidden">' . esc_html__( 'Removed', 'wp-admin-theme-cd' ) . '</span>';
				}

				echo '<input type="checkbox" ' . $checked . ' id="' . $field . '" name="wp_admin_theme_settings_options[' . $field . ']" />';

				echo '<label for="' . $field . '">' . esc_html__( 'Disable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			if( $field == 'disable_page_ms' ) {            
				if( ! is_multisite() ) { 
					echo '</div>';            

					echo '<small class="wpat-info">' . esc_html__( 'Activate multisite support for WordPress to use this option', 'wp-admin-theme-cd' ) . '.</small>';
				}
			}


		} 

		// disable theme options (multisite)

		public function admin_theme_disable_theme_options_settings() {

			if( $this->options['disable_theme_options'] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options['disable_theme_options']) { 
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Deactivated', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status visible">' . esc_html__( 'Activated', 'wp-admin-theme-cd' ) . '</span>';
			}

			global $blog_id;

			if ( is_multisite() && $blog_id == 1 ) {
				echo '<input type="checkbox" ' . $checked . ' id="disable_theme_options" name="wp_admin_theme_settings_options[disable_theme_options]" />';
			} else {
				echo '<input type="checkbox" ' . $checked . ' id="#" name="#" disabled="disabled" />';
			}

			echo '<label for="disable_theme_options">' . esc_html__( 'Disable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . esc_html__( 'Disable the permissions to change WP Admin Theme options for all other network sites', 'wp-admin-theme-cd' ) . '.</p>';

			if ( ! is_multisite() ) {
				echo '<small class="wpat-info">' . esc_html__( 'Activate multisite support for WordPress to use this option', 'wp-admin-theme-cd' ) . '.</small>';
			} 

		}

		// wp optimization
		/*****************************************************************/

		public function admin_theme_wp_optimization_settings() {

			// get all optimization settings fields
			$field = array_shift( $this->optimization_option_fields );

			if( $this->options[$field[0]] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options[$field[0]] ) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Activated', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Removed', 'wp-admin-theme-cd' ) . '</span>';
			}          

			echo '<input type="checkbox" ' . $checked . ' id="' . $field[0] . '" name="wp_admin_theme_settings_options[' . $field[0] . ']" />';

			echo '<label for="' . $field[0] . '">' . esc_html__( 'Disable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . $field[1] . '</p>';

			echo '<small class="wpat-info">' . $field[2] . '</small>';

		}

		// remove wp meta boxes
		/*****************************************************************/

		function admin_theme_meta_box_settings() {

			// get all meta box settings fields
			$field = array_shift( $this->meta_box_option_fields );

			if( $this->options[$field[0]] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options[$field[0]] ) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Activated', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Removed', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="' . $field[0] . '" name="wp_admin_theme_settings_options[' . $field[0] . ']" />';

			echo '<label for="' . $field[0] . '">' . esc_html__( 'Disable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			echo '<p class="description">' . $field[1] . '</p>';

			if( $field[2] ) {
				echo '<small class="wpat-info">' . $field[2] . '</small>';
			}

		} 

		// remove wp dashboard widgets
		/*****************************************************************/

		function admin_theme_db_widgets_settings() {

			// get all meta box settings fields
			$field = array_shift( $this->db_widget_option_fields );        

			// System info dashboad widget can not be activated, if the plugin system info page is deactivated
			$plugin_system_page_is_disabled = $this->options['disable_page_system'];

			if( $field == 'dbw_wpat_sys_info' ) {
				if( $plugin_system_page_is_disabled ) { 
					echo '<div class="wpat-inactive-option">';
				}
			}

				if( $this->options[$field] ) $checked = ' checked="checked" '; 
				else $checked = '';

				if( $field == 'dbw_wpat_sys_info' && $plugin_system_page_is_disabled ) {
					$field_status = '<span class="field-status hidden">' . esc_html__( 'Removed', 'wp-admin-theme-cd' ) . '</span>';
				} elseif( ! $this->options[$field] ) { 
					$field_status = '<span class="field-status visible">' . esc_html__( 'Activated', 'wp-admin-theme-cd' ) . '</span>';
				} else {
					$field_status = '<span class="field-status hidden">' . esc_html__( 'Removed', 'wp-admin-theme-cd' ) . '</span>';
				}

				echo '<input type="checkbox" ' . $checked . ' id="' . $field . '" name="wp_admin_theme_settings_options[' . $field . ']" />';

				echo '<label for="' . $field . '">' . esc_html__( 'Disable', 'wp-admin-theme-cd' ) . $field_status . '</label>';


			if( $field == 'dbw_wpat_sys_info' ) {

				if( $plugin_system_page_is_disabled ) { 
					echo '</div>';            

					echo '<small class="wpat-info">' . esc_html__( 'System info dashboad widget can not be activated, if the plugin system info page is deactivated', 'wp-admin-theme-cd' ) . '.</small>';
				}
			}


		} 

		// remove wp widgets
		/*****************************************************************/

		function admin_theme_widgets_settings() {

			// get all meta box settings fields
			$field = array_shift( $this->widget_option_fields );

			if( $this->options[$field] ) $checked = ' checked="checked" '; 
			else $checked = '';

			if( ! $this->options[$field] ) { 
				$field_status = '<span class="field-status visible">' . esc_html__( 'Activated', 'wp-admin-theme-cd' ) . '</span>';
			} else {
				$field_status = '<span class="field-status hidden">' . esc_html__( 'Removed', 'wp-admin-theme-cd' ) . '</span>';
			}

			echo '<input type="checkbox" ' . $checked . ' id="' . $field . '" name="wp_admin_theme_settings_options[' . $field . ']" />';

			echo '<label for="' . $field . '">' . esc_html__( 'Disable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

		} 

		// frontend
		/*****************************************************************/

		public function admin_theme_frontend_settings() {

			// Get all frontend settings fields
			$field = array_shift( $this->frontend_option_fields );

			// Meta Referrer Policy Field
			if( $field[0] == 'meta_referrer_policy' ) {

				$items = array(
					'none' => esc_html__( 'Disabled', 'wp-admin-theme-cd' ),
					'no-referrer' => 'No Referrer',
					'no-referrer-when-downgrade' => 'No Referrer When Downgrade',
					'same-origin' => 'Same Origin',
					'origin' => 'Origin',
					'strict-origin' => 'Strict Origin',
					'origin-when-crossorigin' => 'Origin When Crossorigin',
					'strict-origin-when-crossorigin' => 'Strict Origin When Crossorigin',
					'unsafe-url' => 'Unsafe URL',
				);

				echo '<select id="meta_referrer_policy" name="wp_admin_theme_settings_options[meta_referrer_policy]">';

					foreach( $items as $key => $item ) {

						$selected = ( $this->options['meta_referrer_policy'] == $key ) ? 'selected="selected"' : '';

						echo '<option value="' . esc_html( $key ) . '" ' . esc_html( $selected ) . '>' . esc_html( $item ) . '</option>';
					}

				echo '</select>';

			// Header + Footer Code
			} elseif( $field[0] == 'wp_header_code' || $field[0] == 'wp_footer_code' ) {

				$val = ( isset( $this->options[$field[0]] ) ) ? $this->options[$field[0]] : '';
				echo '<textarea class="option-textarea" type="text" name="wp_admin_theme_settings_options[' . $field[0] . ']" placeholder="<script>alert(\'My custom script\');</script> or <style>.my-class {color: red}</style>" />' . $val . '</textarea>';

			// Other Fields
			} else {  

				if( $this->options[$field[0]] ) $checked = ' checked="checked" '; 
				else $checked = '';

				if( ! $this->options[$field[0]] ) { 
					$field_status = '<span class="field-status visible">' . esc_html__( 'Activated', 'wp-admin-theme-cd' ) . '</span>';
				} else {
					$field_status = '<span class="field-status hidden">' . esc_html__( 'Removed', 'wp-admin-theme-cd' ) . '</span>';
				}          

				echo '<input type="checkbox" ' . $checked . ' id="' . $field[0] . '" name="wp_admin_theme_settings_options[' . $field[0] . ']" />';

				echo '<label for="' . $field[0] . '">' . esc_html__( 'Disable', 'wp-admin-theme-cd' ) . $field_status . '</label>';

			}

			echo '<p class="description">' . $field[1] . '</p>';

			echo '<small class="wpat-info">' . $field[2] . '</small>';

		}

	} // end class

	WP_Admin_Theme_CD_Options::get_instance();


	/*****************************************************************/
	/* CREATE OPTION WRAPPER FUNCTION */
	/*****************************************************************/

	if ( ! function_exists( 'wpat_option' ) ) :

		function wpat_option( $option ) {

			// Get currently indexed option fields
			$options = get_option( 'wp_admin_theme_settings_options' );
			if( is_multisite() ) {
				$options = get_blog_option( get_current_blog_id(), 'wp_admin_theme_settings_options', array() );
			}
			
			$get_option = isset( $options[$option] ) ? $options[$option] : false;
			
			return $get_option;

		}

	endif;


	/*****************************************************************/
	/* SWITCH HEX COLOR CODE TO RGBA COLOR CODE */
	/*****************************************************************/

	if ( ! function_exists( 'wp_admin_theme_cd_hex2rgba' ) ) :

		function wp_admin_theme_cd_hex2rgba( $color, $opacity = false ) {

			$default = 'rgb(0,0,0)';

			// Return default if no color provided
			if( empty( $color ) ) {
				return $default;
			}

			// Sanitize $color if "#" is provided 
			if( $color[0] == '#' ) {
				$color = substr( $color, 1 );
			}

			// Check if color has 6 or 3 characters and get values
			if( strlen( $color ) == 6 ) {
				$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
			} elseif ( strlen( $color ) == 3 ) {
				$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
			} else {
				return $default;
			}

			// Convert hexadec to rgb
			$rgb = array_map( 'hexdec', $hex );

			// Check if opacity is set(rgba or rgb)
			if( $opacity ) {
				if( abs( $opacity ) > 1 ) $opacity = 1.0;
				$output = 'rgba(' . implode( ",", $rgb ) . ',' . $opacity . ')';
			} else {
				$output = 'rgb(' . implode( ",", $rgb ) . ')';
			}

			// Return rgb(a) color string
			return $output;

			/* Usage example:

			$color = '#ffa226';
			$rgb = hex2rgba($color);
			$rgba = hex2rgba($color, 0.7);

			*/

		}

	endif;


	/*****************************************************************/
	/* GET INSTALLATION URL */
	/*****************************************************************/

	if ( ! function_exists( ' wp_admin_theme_cd_root_url' ) ) : 

		// Return the root url
		function wp_admin_theme_cd_root_url() {

			// For WordPress installation use this snippet to get the root url
			$url_parts = parse_url( home_url() );
			$domain = $url_parts['host'];

			return set_url_scheme( 'http' . ( empty( $_SERVER['HTTPS'] ) ? '' : 's' ) . '://' . $domain );

			// Notice: The following snippet is not working in all cases
			// --> return set_url_scheme( 'http' . ( empty( $_SERVER['HTTPS'] ) ? '' : 's' ) . '://' . $_SERVER['HTTP_HOST'] );

		}

	endif;


	/*****************************************************************/
	/* MYSQLI CONNECTION */
	/*****************************************************************/

	if ( ! function_exists( ' wp_admin_theme_cd_mysqli_connect' ) ) : 

		// Connect to the WordPress MySQL database
		function wp_admin_theme_cd_mysqli_connect() {
			
			return mysqli_connect( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME );

		}

	endif;


	/*****************************************************************/
	/* PLUGIN USAGE */
	/*****************************************************************/

	function wp_admin_theme_cd_plugin() {	
		$permission = 'denied';	
		if( has_filter('wp_admin_theme_cd_accepted') ) {
			$apply = apply_filters('wp_admin_theme_cd_accepted', $permission);
			if( $apply === 'accepted_by_theme' || $apply === 'accepted' ) {
				return true;
			}
		}	
		return false;
	}

	add_action( 'admin_init', 'wp_admin_theme_cd_plugin' );


	/*****************************************************************/
	/* PLUGIN ENVATO RATING */
	/*****************************************************************/

	if ( ! function_exists('wp_admin_theme_cd_activation_date') ) :	

		function wp_admin_theme_cd_activation_date() {
			
			// Set plugin activation date
			add_option('wp_admin_theme_cd_activation_date', current_time( 'mysql' ), '', 'yes');
			define( 'WP_ADMIN_THEME_CD_ACTIVATION_DATE', get_option('wp_admin_theme_cd_activation_date') );
			
		}

	endif;

	add_action('plugins_loaded', 'wp_admin_theme_cd_activation_date');


	if ( ! function_exists('wp_admin_theme_cd_envato_rating') ) :	

		function wp_admin_theme_cd_envato_rating() {
			
			$start_date = WP_ADMIN_THEME_CD_ACTIVATION_DATE;  
			$date = strtotime( $start_date );
			$date = strtotime( '+182 day', $date ); // after a half year of plugin activation
			$reached_time = date( 'Y-m-d H:i:s', $date );		

			// Check if plugin activation date is older than a half year
			if( $reached_time < current_time( 'mysql' ) ) {

				$apply = apply_filters('wp_admin_theme_cd_accepted', $permission);
				if( $apply === 'accepted_by_theme' ) {
					// Do not show this admin notice, if the plugin is free in combination with "CreativeDive" themes
					return false;
				}

				if ( ! function_exists( 'wp_admin_theme_cd_envato_rating_notice' ) ) :

					// Show a admin notice to rate for the theme		
					function  wp_admin_theme_cd_envato_rating_notice() { 

						$star = '<span style="font-size:17px;margin:0px -2px;color:rgba(208,174,71,0.57)" class="dashicons dashicons-star-filled"></span>';
						$author = '<img style="float:left;margin:-6px 10px 0px -6px;border-radius:50%" class="theme-author-img" src="' . wp_admin_theme_cd_path( "/img/avatar-author.jpg" ) . '" width="32" alt="Theme Author">'; ?>

						<div class="notice notice-success is-dismissible">
							<p><?php printf( wp_kses_post( __( '%3$s <strong style="color:#4d820c">Hey you! I\'m Martin, the plugin author of WP Admin Theme CD.</strong> Do you like this plugin? Please show your appreciation and rate the plugin. Help me to develop a powerful plugin that will benefit you for a long time. %2$s %2$s %2$s %2$s %2$s <a href="%1$s" target="_blank">Rate now!</a>', 'hannah-cd' ) ), WP_ADMIN_THEME_CD_ENVATO_THEME_REVIEW_URL, $star, $author ); ?></p>
						</div>

					<?php }

				endif;

				add_action('admin_notices', 'wp_admin_theme_cd_envato_rating_notice');

			}

		}

	endif;

	add_action('admin_init', 'wp_admin_theme_cd_envato_rating');


	/*****************************************************************/
	/* PLUGIN ACTIVATION MESSAGE */
	/*****************************************************************/

	if ( ! function_exists( 'wp_admin_theme_cd_plugin_activation_message' ) ) :

		function wp_admin_theme_cd_plugin_activation_message() {			
			return esc_html__( 'This plugin is not activated. Please enter your Envato purchase code to enable the plugin settings.', 'wp-admin-theme-cd' );
		}

	endif;


	/*****************************************************************/
	/* INCLUDE PLUGIN PARTS */
	/*****************************************************************/

	// Plugin Setup Page
	include_once( wp_admin_theme_cd_dir( 'inc/page-setup.php' ) );
	include_once( wp_admin_theme_cd_dir( 'inc/option-setup.php' ) );

	// Plugin System Info Page
	if( wpat_option('disable_page_system') != true ) { 
		include_once( wp_admin_theme_cd_dir( 'inc/page-system-info.php' ) );
	}

	// Plugin Im- / Export Page
	if( wpat_option('disable_page_export') != true ) { 
		include_once( wp_admin_theme_cd_dir( 'inc/page-ex-import.php' ) );
	}

	// Plugin Multisite Sync Page
	if( wpat_option('disable_page_ms') != true ) { 
		include_once( wp_admin_theme_cd_dir( 'inc/page-multisite-sync.php' ) );
	}

	// (BETA) Plugin Optimization Tipps Page
	//include_once( wp_admin_theme_cd_dir( 'inc/page-optimization.php' ) );
	
	include_once( wp_admin_theme_cd_dir( 'inc/option-db-widgets.php' ) );
	include_once( wp_admin_theme_cd_dir( 'inc/option-layout.php' ) );	
	include_once( wp_admin_theme_cd_dir( 'inc/option-login.php' ) );
	include_once( wp_admin_theme_cd_dir( 'inc/option-footer.php' ) );
	include_once( wp_admin_theme_cd_dir( 'inc/option-wp.php' ) );


	/*****************************************************************/
	/* LOAD FRONTEND PART */
	/*****************************************************************/

	if ( ! function_exists( 'wp_admin_theme_cd_load_frontend_part' ) ) :

		function wp_admin_theme_cd_load_frontend_part() {	
			if( is_user_logged_in() ) {
				include_once( wp_admin_theme_cd_dir( 'inc/option-frontend.php' ) );
			}
		}

	endif;

	add_action('init', 'wp_admin_theme_cd_load_frontend_part');

	
	/*****************************************************************/
	/* PLUGIN MAIN PAGE REDIRECT TO PLUGIN ACTIVATION PAGE */
	/*****************************************************************/	
		
	function wp_admin_theme_cd_activation_redirect() {

		if( wp_admin_theme_cd_activation_status() ) {
			return false;
		}
		
		// Check current page is "wp-admin-theme-cd"
		if( isset( $_GET['page'] ) && $_GET['page'] == 'wp-admin-theme-cd' ) {
			// Redirect to plugin "wp-admin-theme-cd-purchase-code&tab=activation" page to verify the plugin
			wp_redirect( admin_url('tools.php?page=wp-admin-theme-cd-purchase-code&tab=activation') );
			exit();
		}
		
	}

	add_action('admin_init', 'wp_admin_theme_cd_activation_redirect', 1);


	/*****************************************************************/
	/* SET WP LESS OPTION VARIABLES */
	/*****************************************************************/

	// WP Less
	if( ! class_exists( 'wp_less' ) ) {
		// Only included if wpless is not already loaded by another plugin or theme to prevent conflicts
		include_once( wp_admin_theme_cd_dir('inc/wp-less/wp-less.php') );
	}	

	if ( ! function_exists( 'wp_admin_theme_cd_wp_less_vars' ) ) :

		function wp_admin_theme_cd_wp_less_vars( $vars, $handle ) {

			$vars[ 'wpatThemeColor' ] = '#4777CD';
			if( wpat_option('theme_color') ) {
				$vars[ 'wpatThemeColor' ] = wpat_option('theme_color');
			}
			
			$vars[ 'wpatGradientStartColor' ] = '#545c63';
			if( wpat_option('theme_background') ) {
				$vars[ 'wpatGradientStartColor' ] = wp_admin_theme_cd_hex2rgba( wpat_option('theme_background') );
			}
			
			$vars[ 'wpatGradientEndColor' ] = '#32373c';
			if( wpat_option('theme_background_end') ) {
				$vars[ 'wpatGradientEndColor' ] = wp_admin_theme_cd_hex2rgba( wpat_option('theme_background_end') );
			}
			
			$vars[ 'wpatToolbarColor' ] = '#32373c';
			if( wpat_option('toolbar_color') ) {
				$vars[ 'wpatToolbarColor' ] = wpat_option('toolbar_color');
			}
			
			$vars[ 'wpatSpacingMaxWidth' ] = '2000px';
			if( wpat_option('spacing_max_width') ) {
				$vars[ 'wpatSpacingMaxWidth' ] = wpat_option('spacing_max_width') . 'px';
			}
			
			$vars[ 'wpatMenuLeftWidth' ] = '160px';
			if( wpat_option('left_menu_width') ) {
				$vars[ 'wpatMenuLeftWidth' ] = wpat_option('left_menu_width') . 'px';
			}
			
			$vars[ 'wpatMenuLeftWidthDiff' ] = 160 - 40 . 'px';
			if( wpat_option('left_menu_width') ) {
				$vars[ 'wpatMenuLeftWidthDiff' ] = wpat_option('left_menu_width') - 40 . 'px';
			}
			
			$vars[ 'wpatLoginLogoSize' ] = '200px';
			if( wpat_option('logo_size') ) {
				$vars[ 'wpatLoginLogoSize' ] = wpat_option('logo_size') . 'px';
			}
			
			$vars[ 'wpatToolbarIcon' ] = 'none';
			if( wpat_option('toolbar_icon') != '' ) {
				$vars[ 'wpatToolbarIcon' ] = 'url(' . wpat_option('toolbar_icon') . ')';
			}

			$vars[ 'wpatWebFont' ] = 'none';
			if( wpat_option('google_webfont') != '' ) {
				$web_font = str_replace( '+', ' ', esc_html( wpat_option('google_webfont') ) );
				$vars[ 'wpatWebFont' ] = $web_font;
			}

			$vars[ 'wpatLoginBg' ] = 'none';
			if( wpat_option('login_bg') != '' ) {
				$vars[ 'wpatLoginBg' ] = 'url(' . wpat_option('login_bg') . ')';
			}

			$vars[ 'wpatLoginLogo' ] = 'none';
			if( wpat_option('logo_upload') != '' ) {
				$vars[ 'wpatLoginLogo' ] = 'url(' . wpat_option('logo_upload') . ')';
			}			

			return $vars;

		}

	endif;


	/*****************************************************************/
	/* PARSE WP LESS ONLY IN SPECIFIC CASES */
	/*****************************************************************/

	if ( ! function_exists( 'wp_admin_theme_cd_load_wpless_filter' ) ) :

		function wp_admin_theme_cd_load_wpless_filter( $hook ) {

			// Method to get the admin page hook
			// wp_die($hook);

			// Function to check if is wp login or wp register page
			$is_login = wp_admin_theme_cd_is_login_page();

			// Load only on admin_toplevel_page?page=mypluginname
			$is_wpat_option_page = ( $hook != 'tools_page_wp-admin-theme-cd' );

			// Load filter only in above cases
			if( $is_login || is_admin() && $is_wpat_option_page ) {
				add_filter( 'less_vars', 'wp_admin_theme_cd_wp_less_vars', 999, 2 );
			}

		}

	endif;

	add_action( 'init', 'wp_admin_theme_cd_load_wpless_filter' );
    
endif; // END of class_exists check

?>