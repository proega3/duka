<?php
/*
Plugin Name: Disable WooCommerce REST API authentication
Plugin URI: https://www.damiencarbery.com/2019/07/disable-woocommerce-rest-api-authentication/
Description: Override WooCommerce capability check so that all REST API queries are allowed.
Author: Damien Carbery
Version: 0.1

*/

add_filter( 'woocommerce_rest_check_permissions', 'dcwd_allow_rest_api_queries', 10, 4 ); 
function dcwd_allow_rest_api_queries( $permission, $context, $zero, $object ) {
	// Optionally limit permitted queries to different contexts.
	/*if ( 'read' != $context ) {
		return $permission;
	}*/
	// Write the parameters to the error log (or debug.log file) to see what requests are being accessed.
	//error_log( sprintf( 'Permission: %s, Context: %s; Object: %s', var_export( $permission, true ), $context, var_export( $object, true ) ) );

	return true;  // Allow all queries.
}
